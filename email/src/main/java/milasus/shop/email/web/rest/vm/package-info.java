/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.email.web.rest.vm;
