package milasus.shop.email.web.rest;

import milasus.shop.email.exception.IncorrectEMailAddressRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import person.Person;

/**
 * EmailResource controller
 */
@RestController
@RequestMapping(value = "/api/email")
public class EmailResource {

    private final Logger log = LoggerFactory.getLogger(EmailResource.class);

    /**
     * POST Request sendEmail
     * @param person
     * @return
     */
    @PostMapping("/send-email")
    public String sendEmail(@RequestBody Person person) {

        this.checkEMailAddress(person);

        return "E-Mail has been sent successfully";
    }

    private void checkEMailAddress(Person person) {
        if(person.geteMailAddress() == null || !person.geteMailAddress().contains("@")
            || !person.geteMailAddress().contains(".") ) {
            throw new IncorrectEMailAddressRuntimeException("E-Mail Address is not correct");
        }
    }

}
