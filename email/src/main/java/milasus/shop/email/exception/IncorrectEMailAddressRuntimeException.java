package milasus.shop.email.exception;

/**
 * Error thrown symbolises an incorrect E-Mail address, ex. g. empty
 */
public class IncorrectEMailAddressRuntimeException extends RuntimeException{

    public IncorrectEMailAddressRuntimeException(String errorMessage){
        super(errorMessage);
    }

}
