package milasus.shop.email.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM_ACCOUNT = "system";

    private Constants() {
    }
}
