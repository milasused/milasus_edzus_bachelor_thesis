package milasus.shop.email.web.rest;

import milasus.shop.email.EmailApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the EmailResource REST controller.
 *
 * @see EmailResource
 */
@SpringBootTest(classes = EmailApp.class)
public class EmailResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        EmailResource emailResource = new EmailResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(emailResource)
            .build();
    }

    /**
     * Test sendEmail
     */
    @Test
    public void testSendEmail() throws Exception {
        restMockMvc.perform(post("/api/email/send-email"))
            .andExpect(status().isOk());
    }
}
