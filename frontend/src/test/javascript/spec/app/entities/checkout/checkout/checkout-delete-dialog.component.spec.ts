/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { FrontendTestModule } from '../../../../test.module';
import { CheckoutDeleteDialogComponent } from 'app/entities/checkout/checkout/checkout-delete-dialog.component';
import { CheckoutService } from 'app/entities/checkout/checkout/checkout.service';

describe('Component Tests', () => {
  describe('Checkout Management Delete Component', () => {
    let comp: CheckoutDeleteDialogComponent;
    let fixture: ComponentFixture<CheckoutDeleteDialogComponent>;
    let service: CheckoutService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontendTestModule],
        declarations: [CheckoutDeleteDialogComponent]
      })
        .overrideTemplate(CheckoutDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CheckoutDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CheckoutService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete('123');
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith('123');
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
