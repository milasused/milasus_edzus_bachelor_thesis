/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FrontendTestModule } from '../../../../test.module';
import { CheckoutDetailComponent } from 'app/entities/checkout/checkout/checkout-detail.component';
import { Checkout } from 'app/shared/model/checkout/checkout.model';

describe('Component Tests', () => {
  describe('Checkout Management Detail Component', () => {
    let comp: CheckoutDetailComponent;
    let fixture: ComponentFixture<CheckoutDetailComponent>;
    const route = ({ data: of({ checkout: new Checkout('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontendTestModule],
        declarations: [CheckoutDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CheckoutDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CheckoutDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.checkout).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
