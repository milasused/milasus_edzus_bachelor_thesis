/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { CheckoutService } from 'app/entities/checkout/checkout/checkout.service';
import { ICheckout, Checkout } from 'app/shared/model/checkout/checkout.model';

describe('Service Tests', () => {
  describe('Checkout Service', () => {
    let injector: TestBed;
    let service: CheckoutService;
    let httpMock: HttpTestingController;
    let elemDefault: ICheckout;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(CheckoutService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Checkout('ID', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find('123')
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Checkout', async () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new Checkout(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Checkout', async () => {
        const returnedFromService = Object.assign(
          {
            eMail: 'BBBBBB',
            firstName: 'BBBBBB',
            lastName: 'BBBBBB',
            street: 'BBBBBB',
            zipCode: 'BBBBBB',
            city: 'BBBBBB',
            country: 'BBBBBB',
            creditCartNumber: 'BBBBBB',
            creditCartSecurityNumber: 1,
            creditCardExpirationMonth: 1,
            creditCardExpirationYear: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Checkout', async () => {
        const returnedFromService = Object.assign(
          {
            eMail: 'BBBBBB',
            firstName: 'BBBBBB',
            lastName: 'BBBBBB',
            street: 'BBBBBB',
            zipCode: 'BBBBBB',
            city: 'BBBBBB',
            country: 'BBBBBB',
            creditCartNumber: 'BBBBBB',
            creditCartSecurityNumber: 1,
            creditCardExpirationMonth: 1,
            creditCardExpirationYear: 1
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Checkout', async () => {
        const rxPromise = service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
