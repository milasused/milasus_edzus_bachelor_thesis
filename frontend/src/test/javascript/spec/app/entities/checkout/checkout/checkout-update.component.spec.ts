/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { FrontendTestModule } from '../../../../test.module';
import { CheckoutUpdateComponent } from 'app/entities/checkout/checkout/checkout-update.component';
import { CheckoutService } from 'app/entities/checkout/checkout/checkout.service';
import { Checkout } from 'app/shared/model/checkout/checkout.model';

describe('Component Tests', () => {
  describe('Checkout Management Update Component', () => {
    let comp: CheckoutUpdateComponent;
    let fixture: ComponentFixture<CheckoutUpdateComponent>;
    let service: CheckoutService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontendTestModule],
        declarations: [CheckoutUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CheckoutUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CheckoutUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CheckoutService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Checkout('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Checkout();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
