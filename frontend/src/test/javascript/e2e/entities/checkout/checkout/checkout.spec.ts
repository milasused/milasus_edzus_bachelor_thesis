/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { CheckoutComponentsPage, CheckoutDeleteDialog, CheckoutUpdatePage } from './checkout.page-object';

const expect = chai.expect;

describe('Checkout e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let checkoutUpdatePage: CheckoutUpdatePage;
  let checkoutComponentsPage: CheckoutComponentsPage;
  let checkoutDeleteDialog: CheckoutDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Checkouts', async () => {
    await navBarPage.goToEntity('checkout');
    checkoutComponentsPage = new CheckoutComponentsPage();
    await browser.wait(ec.visibilityOf(checkoutComponentsPage.title), 5000);
    expect(await checkoutComponentsPage.getTitle()).to.eq('Checkouts');
  });

  it('should load create Checkout page', async () => {
    await checkoutComponentsPage.clickOnCreateButton();
    checkoutUpdatePage = new CheckoutUpdatePage();
    expect(await checkoutUpdatePage.getPageTitle()).to.eq('Create or edit a Checkout');
    await checkoutUpdatePage.cancel();
  });

  it('should create and save Checkouts', async () => {
    const nbButtonsBeforeCreate = await checkoutComponentsPage.countDeleteButtons();

    await checkoutComponentsPage.clickOnCreateButton();
    await promise.all([
      checkoutUpdatePage.setEMailInput('eMail'),
      checkoutUpdatePage.setFirstNameInput('firstName'),
      checkoutUpdatePage.setLastNameInput('lastName'),
      checkoutUpdatePage.setStreetInput('street'),
      checkoutUpdatePage.setZipCodeInput('zipCode'),
      checkoutUpdatePage.setCityInput('city'),
      checkoutUpdatePage.setCountryInput('country'),
      checkoutUpdatePage.setCreditCartNumberInput('creditCartNumber'),
      checkoutUpdatePage.setCreditCartSecurityNumberInput('5'),
      checkoutUpdatePage.setCreditCardExpirationMonthInput('5'),
      checkoutUpdatePage.setCreditCardExpirationYearInput('5')
    ]);
    expect(await checkoutUpdatePage.getEMailInput()).to.eq('eMail', 'Expected EMail value to be equals to eMail');
    expect(await checkoutUpdatePage.getFirstNameInput()).to.eq('firstName', 'Expected FirstName value to be equals to firstName');
    expect(await checkoutUpdatePage.getLastNameInput()).to.eq('lastName', 'Expected LastName value to be equals to lastName');
    expect(await checkoutUpdatePage.getStreetInput()).to.eq('street', 'Expected Street value to be equals to street');
    expect(await checkoutUpdatePage.getZipCodeInput()).to.eq('zipCode', 'Expected ZipCode value to be equals to zipCode');
    expect(await checkoutUpdatePage.getCityInput()).to.eq('city', 'Expected City value to be equals to city');
    expect(await checkoutUpdatePage.getCountryInput()).to.eq('country', 'Expected Country value to be equals to country');
    expect(await checkoutUpdatePage.getCreditCartNumberInput()).to.eq(
      'creditCartNumber',
      'Expected CreditCartNumber value to be equals to creditCartNumber'
    );
    expect(await checkoutUpdatePage.getCreditCartSecurityNumberInput()).to.eq(
      '5',
      'Expected creditCartSecurityNumber value to be equals to 5'
    );
    expect(await checkoutUpdatePage.getCreditCardExpirationMonthInput()).to.eq(
      '5',
      'Expected creditCardExpirationMonth value to be equals to 5'
    );
    expect(await checkoutUpdatePage.getCreditCardExpirationYearInput()).to.eq(
      '5',
      'Expected creditCardExpirationYear value to be equals to 5'
    );
    await checkoutUpdatePage.save();
    expect(await checkoutUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await checkoutComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Checkout', async () => {
    const nbButtonsBeforeDelete = await checkoutComponentsPage.countDeleteButtons();
    await checkoutComponentsPage.clickOnLastDeleteButton();

    checkoutDeleteDialog = new CheckoutDeleteDialog();
    expect(await checkoutDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Checkout?');
    await checkoutDeleteDialog.clickOnConfirmButton();

    expect(await checkoutComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
