import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CheckoutComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-checkout div table .btn-danger'));
  title = element.all(by.css('jhi-checkout div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class CheckoutUpdatePage {
  pageTitle = element(by.id('jhi-checkout-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  eMailInput = element(by.id('field_eMail'));
  firstNameInput = element(by.id('field_firstName'));
  lastNameInput = element(by.id('field_lastName'));
  streetInput = element(by.id('field_street'));
  zipCodeInput = element(by.id('field_zipCode'));
  cityInput = element(by.id('field_city'));
  countryInput = element(by.id('field_country'));
  creditCartNumberInput = element(by.id('field_creditCartNumber'));
  creditCartSecurityNumberInput = element(by.id('field_creditCartSecurityNumber'));
  creditCardExpirationMonthInput = element(by.id('field_creditCardExpirationMonth'));
  creditCardExpirationYearInput = element(by.id('field_creditCardExpirationYear'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setEMailInput(eMail) {
    await this.eMailInput.sendKeys(eMail);
  }

  async getEMailInput() {
    return await this.eMailInput.getAttribute('value');
  }

  async setFirstNameInput(firstName) {
    await this.firstNameInput.sendKeys(firstName);
  }

  async getFirstNameInput() {
    return await this.firstNameInput.getAttribute('value');
  }

  async setLastNameInput(lastName) {
    await this.lastNameInput.sendKeys(lastName);
  }

  async getLastNameInput() {
    return await this.lastNameInput.getAttribute('value');
  }

  async setStreetInput(street) {
    await this.streetInput.sendKeys(street);
  }

  async getStreetInput() {
    return await this.streetInput.getAttribute('value');
  }

  async setZipCodeInput(zipCode) {
    await this.zipCodeInput.sendKeys(zipCode);
  }

  async getZipCodeInput() {
    return await this.zipCodeInput.getAttribute('value');
  }

  async setCityInput(city) {
    await this.cityInput.sendKeys(city);
  }

  async getCityInput() {
    return await this.cityInput.getAttribute('value');
  }

  async setCountryInput(country) {
    await this.countryInput.sendKeys(country);
  }

  async getCountryInput() {
    return await this.countryInput.getAttribute('value');
  }

  async setCreditCartNumberInput(creditCartNumber) {
    await this.creditCartNumberInput.sendKeys(creditCartNumber);
  }

  async getCreditCartNumberInput() {
    return await this.creditCartNumberInput.getAttribute('value');
  }

  async setCreditCartSecurityNumberInput(creditCartSecurityNumber) {
    await this.creditCartSecurityNumberInput.sendKeys(creditCartSecurityNumber);
  }

  async getCreditCartSecurityNumberInput() {
    return await this.creditCartSecurityNumberInput.getAttribute('value');
  }

  async setCreditCardExpirationMonthInput(creditCardExpirationMonth) {
    await this.creditCardExpirationMonthInput.sendKeys(creditCardExpirationMonth);
  }

  async getCreditCardExpirationMonthInput() {
    return await this.creditCardExpirationMonthInput.getAttribute('value');
  }

  async setCreditCardExpirationYearInput(creditCardExpirationYear) {
    await this.creditCardExpirationYearInput.sendKeys(creditCardExpirationYear);
  }

  async getCreditCardExpirationYearInput() {
    return await this.creditCardExpirationYearInput.getAttribute('value');
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CheckoutDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-checkout-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-checkout'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
