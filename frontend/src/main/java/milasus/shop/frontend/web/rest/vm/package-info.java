/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.frontend.web.rest.vm;
