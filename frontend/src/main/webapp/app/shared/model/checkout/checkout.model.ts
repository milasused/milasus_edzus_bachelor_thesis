export interface ICheckout {
  id?: string;
  eMail?: string;
  firstName?: string;
  lastName?: string;
  street?: string;
  zipCode?: string;
  city?: string;
  country?: string;
  creditCartNumber?: string;
  creditCartSecurityNumber?: number;
  creditCardExpirationMonth?: number;
  creditCardExpirationYear?: number;
}

export class Checkout implements ICheckout {
  constructor(
    public id?: string,
    public eMail?: string,
    public firstName?: string,
    public lastName?: string,
    public street?: string,
    public zipCode?: string,
    public city?: string,
    public country?: string,
    public creditCartNumber?: string,
    public creditCartSecurityNumber?: number,
    public creditCardExpirationMonth?: number,
    public creditCardExpirationYear?: number
  ) {}
}
