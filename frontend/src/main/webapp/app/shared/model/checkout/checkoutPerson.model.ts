export interface ICheckoutPerson {
  firstName?: string;
  lastName?: string;
  eMailAddress?: string;
  address?: string;
  creditCardNumber?: number;
  creditCardSecurityNumber?: number;
  creditCardExpirationDate?: number;
}

export class CheckoutPerson implements ICheckoutPerson {
  constructor(
    public firstName?: string,
    public lastName?: string,
    public eMailAddress?: string,
    public address?: string,
    public creditCardNumber?: number,
    public creditCardSecurityNumber?: number,
    public creditCardExpirationDate?: number
  ) {}
}
