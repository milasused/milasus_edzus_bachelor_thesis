import { ICheckoutPerson, CheckoutPerson } from './checkoutPerson.model';

export interface IPlaceOrderInformation {
  cartId?: string;
  currency?: string;
  checkoutPerson?: ICheckoutPerson;
}

export class PlaceOrderInformation implements IPlaceOrderInformation {
  constructor(public cartId?: string, public currency?: string, public checkoutPerson?: ICheckoutPerson) {}
}
