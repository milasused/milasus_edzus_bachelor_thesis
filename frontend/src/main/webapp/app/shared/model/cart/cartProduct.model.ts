export interface ICartProduct {
  id?: string;
  price?: number;
}

export class CartProduct implements ICartProduct {
  constructor(public id?: string, public price?: number) {}
}
