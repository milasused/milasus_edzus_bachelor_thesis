import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICart } from 'app/shared/model/cart/cart.model';
import { Cart } from 'app/shared/model/cart/cart.model';
import { ICartProduct } from 'app/shared/model/cart/cartProduct.model';
import { AccountService } from 'app/core';
import { CartService } from './cart.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-cart',
  templateUrl: './cart.component.html'
})
export class CartComponent implements OnInit, OnDestroy {
  carts: ICart[];
  cartProducts: ICartProduct[];
  currentAccount: any;
  eventSubscriber: Subscription;
  responseTest: any;
  singleCart: Cart;

  constructor(
    protected cartService: CartService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    // Reference: https://codecraft.tv/courses/angular/es6-typescript/promises/
    /*const promise = new Promise ((resolve, reject) => {
    setTimeout(() => {*/
    this.cartService
      .query()
      .pipe(
        filter((res: HttpResponse<ICart[]>) => res.ok),
        map((res: HttpResponse<ICart[]>) => res.body)
      )
      .subscribe(
        (res: ICart[]) => {
          this.carts = res;
          this.getCartProducts(this.carts[0].id);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    /*   }, 1000);
    });

    return promise;
    */
    /*
     this.responseTest = this.cartService.getAllProducts
     ('5e0eea7c4d1eae2d088f044e');

     this.onError('Error test, delete');

    // this.singleCart = new Cart(this.carts[0].id);

    this.onError('test');

     this.onError('After');

     this.cartProducts = this.responseTest
     .subscribe(
      (result: ICartProduct[] ) => {
        this.cartProducts = result;
      },
      (result: HttpErrorResponse) => this.onError(result.message)

     );
     */
    /*
    this.cartService.getAllProducts(this.carts[0].id).subscribe((retMessage: ICartProduct[])
    => {
      this.cartProducts = retMessage;
    },
    (retMessage: HttpErrorResponse) => this.onError(retMessage.message)
    );
    */
  }

  ngOnInit() {
    this.loadAll(); // .then(() => this.onError('Task finished'), this.onError('Task failed'));
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCarts();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICart) {
    return item.id;
  }

  registerChangeInCarts() {
    this.eventSubscriber = this.eventManager.subscribe('cartListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  getCartProducts(cartId: string) {
    this.responseTest = this.cartService.getAllProducts(cartId);

    this.responseTest.subscribe(
      (result: ICartProduct[]) => {
        // this.onError('result length: ' + result.length);
        this.cartProducts = result;
      },
      (result: HttpErrorResponse) => this.onError(result.message)
    );
  }

  trackCartProductId(index: number, item: ICartProduct) {
    return item.id;
  }

  getSomething(text: string) {
    return 'something ' + text;
  }
}
