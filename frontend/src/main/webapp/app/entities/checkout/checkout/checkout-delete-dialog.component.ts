import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICheckout } from 'app/shared/model/checkout/checkout.model';
import { CheckoutService } from './checkout.service';

@Component({
  selector: 'jhi-checkout-delete-dialog',
  templateUrl: './checkout-delete-dialog.component.html'
})
export class CheckoutDeleteDialogComponent {
  checkout: ICheckout;

  constructor(protected checkoutService: CheckoutService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.checkoutService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'checkoutListModification',
        content: 'Deleted an checkout'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-checkout-delete-popup',
  template: ''
})
export class CheckoutDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ checkout }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CheckoutDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.checkout = checkout;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/checkout', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/checkout', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
