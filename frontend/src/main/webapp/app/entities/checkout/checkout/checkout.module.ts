import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontendSharedModule } from 'app/shared';
import {
  CheckoutComponent,
  CheckoutDetailComponent,
  CheckoutUpdateComponent,
  CheckoutDeletePopupComponent,
  CheckoutDeleteDialogComponent,
  checkoutRoute,
  checkoutPopupRoute
} from './';

const ENTITY_STATES = [...checkoutRoute, ...checkoutPopupRoute];

@NgModule({
  imports: [FrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CheckoutComponent,
    CheckoutDetailComponent,
    CheckoutUpdateComponent,
    CheckoutDeleteDialogComponent,
    CheckoutDeletePopupComponent
  ],
  entryComponents: [CheckoutComponent, CheckoutUpdateComponent, CheckoutDeleteDialogComponent, CheckoutDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CheckoutCheckoutModule {}
