import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICheckout, Checkout } from 'app/shared/model/checkout/checkout.model';
import { CheckoutService } from './checkout.service';

@Component({
  selector: 'jhi-checkout-update',
  templateUrl: './checkout-update.component.html'
})
export class CheckoutUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    eMail: [],
    firstName: [],
    lastName: [null, [Validators.required]],
    street: [],
    zipCode: [],
    city: [],
    country: [],
    creditCartNumber: [],
    creditCartSecurityNumber: [],
    creditCardExpirationMonth: [],
    creditCardExpirationYear: []
  });

  constructor(protected checkoutService: CheckoutService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ checkout }) => {
      this.updateForm(checkout);
    });
  }

  updateForm(checkout: ICheckout) {
    this.editForm.patchValue({
      id: checkout.id,
      eMail: checkout.eMail,
      firstName: checkout.firstName,
      lastName: checkout.lastName,
      street: checkout.street,
      zipCode: checkout.zipCode,
      city: checkout.city,
      country: checkout.country,
      creditCartNumber: checkout.creditCartNumber,
      creditCartSecurityNumber: checkout.creditCartSecurityNumber,
      creditCardExpirationMonth: checkout.creditCardExpirationMonth,
      creditCardExpirationYear: checkout.creditCardExpirationYear
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const checkout = this.createFromForm();
    if (checkout.id !== undefined) {
      this.subscribeToSaveResponse(this.checkoutService.update(checkout));
    } else {
      this.subscribeToSaveResponse(this.checkoutService.create(checkout));
    }
  }

  private createFromForm(): ICheckout {
    return {
      ...new Checkout(),
      id: this.editForm.get(['id']).value,
      eMail: this.editForm.get(['eMail']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      street: this.editForm.get(['street']).value,
      zipCode: this.editForm.get(['zipCode']).value,
      city: this.editForm.get(['city']).value,
      country: this.editForm.get(['country']).value,
      creditCartNumber: this.editForm.get(['creditCartNumber']).value,
      creditCartSecurityNumber: this.editForm.get(['creditCartSecurityNumber']).value,
      creditCardExpirationMonth: this.editForm.get(['creditCardExpirationMonth']).value,
      creditCardExpirationYear: this.editForm.get(['creditCardExpirationYear']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICheckout>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
