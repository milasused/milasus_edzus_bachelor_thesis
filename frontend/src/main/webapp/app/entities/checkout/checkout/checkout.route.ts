import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Checkout } from 'app/shared/model/checkout/checkout.model';
import { CheckoutService } from './checkout.service';
import { CheckoutComponent } from './checkout.component';
import { CheckoutDetailComponent } from './checkout-detail.component';
import { CheckoutUpdateComponent } from './checkout-update.component';
import { CheckoutDeletePopupComponent } from './checkout-delete-dialog.component';
import { ICheckout } from 'app/shared/model/checkout/checkout.model';

@Injectable({ providedIn: 'root' })
export class CheckoutResolve implements Resolve<ICheckout> {
  constructor(private service: CheckoutService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICheckout> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Checkout>) => response.ok),
        map((checkout: HttpResponse<Checkout>) => checkout.body)
      );
    }
    return of(new Checkout());
  }
}

export const checkoutRoute: Routes = [
  {
    path: '',
    component: CheckoutComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkouts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CheckoutDetailComponent,
    resolve: {
      checkout: CheckoutResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkouts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CheckoutUpdateComponent,
    resolve: {
      checkout: CheckoutResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkouts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CheckoutUpdateComponent,
    resolve: {
      checkout: CheckoutResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkouts'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const checkoutPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CheckoutDeletePopupComponent,
    resolve: {
      checkout: CheckoutResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Checkouts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
