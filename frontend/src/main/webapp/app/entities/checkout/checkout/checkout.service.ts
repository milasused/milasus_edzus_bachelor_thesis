import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICheckout } from 'app/shared/model/checkout/checkout.model';

import { IProduct } from 'app/shared/model/productCatalogue/product.model';
import { IPlaceOrderInformation, PlaceOrderInformation } from 'app/shared/model/checkout/placeOrderInformation.model';

type EntityResponseType = HttpResponse<ICheckout>;
type EntityArrayResponseType = HttpResponse<ICheckout[]>;

@Injectable({ providedIn: 'root' })
export class CheckoutService {
  public resourceUrl = SERVER_API_URL + 'services/checkout/api';

  constructor(protected http: HttpClient) {}

  create(checkout: ICheckout): Observable<EntityResponseType> {
    return this.http.post<ICheckout>(this.resourceUrl + '/checkouts', checkout, { observe: 'response' });
  }

  update(checkout: ICheckout): Observable<EntityResponseType> {
    return this.http.put<ICheckout>(this.resourceUrl + '/checkouts', checkout, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ICheckout>(`${this.resourceUrl + '/checkouts'}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICheckout[]>(this.resourceUrl + '/checkouts', { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl + '/checkouts'}/${id}`, { observe: 'response' });
  }

  goToCheckout(cartId: string) {
    return this.http.post(this.resourceUrl + '/go-to-checkout', cartId);
  }

  emptyCart(cartId: string) {
    return this.http.delete(`${this.resourceUrl + '/empty-cart'}/${cartId}`);
  }

  placeOrder(placeOrderInformation: IPlaceOrderInformation) {
    return this.http.post(this.resourceUrl + '/place-order', placeOrderInformation, { responseType: 'text' });
  }
}
