import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICheckout } from 'app/shared/model/checkout/checkout.model';

@Component({
  selector: 'jhi-checkout-detail',
  templateUrl: './checkout-detail.component.html'
})
export class CheckoutDetailComponent implements OnInit {
  checkout: ICheckout;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ checkout }) => {
      this.checkout = checkout;
    });
  }

  previousState() {
    window.history.back();
  }
}
