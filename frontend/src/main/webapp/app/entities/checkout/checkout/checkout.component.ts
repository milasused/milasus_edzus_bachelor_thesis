import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ICheckout } from 'app/shared/model/checkout/checkout.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { CheckoutService } from './checkout.service';

import { ICart } from 'app/shared/model/cart/cart.model';
import { CartService } from 'app/entities/cart/cart/cart.service';
import { IProduct } from 'app/shared/model/productCatalogue/product.model';

import { IPlaceOrderInformation, PlaceOrderInformation } from 'app/shared/model/checkout/placeOrderInformation.model';
import { ICheckoutPerson, CheckoutPerson } from 'app/shared/model/checkout/checkoutPerson.model';

// Reference: https://stackoverflow.com/questions/47827634/how-to-read-form-values-in-controller/47827703
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-checkout',
  templateUrl: './checkout.component.html'
})
export class CheckoutComponent implements OnInit, OnDestroy {
  checkouts: ICheckout[];
  currentAccount: any;
  eventSubscriber: Subscription;
  itemsPerPage: number;
  links: any;
  page: any;
  predicate: any;
  reverse: any;
  totalItems: number;

  orderForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  eMailAddress: FormControl;
  address: FormControl;
  creditCardNumber: FormControl;
  creditCardSecurityNumber: FormControl;
  creditCardExpirationDate: FormControl;
  paymentCurrency: FormControl;

  activeCartId: string;
  carts: ICart[];
  products: IProduct[];

  placeOrderInformation: IPlaceOrderInformation;
  checkoutPerson: ICheckoutPerson;
  currency: string;
  orderPlacedText: string;

  constructor(
    protected checkoutService: CheckoutService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    protected cartService: CartService,
    protected formBuilder: FormBuilder
  ) {
    this.checkouts = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.reverse = true;
    this.activeCartId = '';
    this.products = [];

    this.firstName = new FormControl('');
    this.lastName = new FormControl('', [Validators.required]);
    this.eMailAddress = new FormControl('', [Validators.required]);
    this.address = new FormControl('', [Validators.required]);
    this.creditCardNumber = new FormControl(null, [Validators.required]);
    this.creditCardSecurityNumber = new FormControl(null, [Validators.required]);
    this.creditCardExpirationDate = new FormControl(null, [Validators.required]);
    this.paymentCurrency = new FormControl('EUR', [Validators.required]);

    this.orderForm = formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      eMailAddress: this.eMailAddress,
      address: this.address,
      creditCardNumber: this.creditCardNumber,
      creditCardSecurityNumber: this.creditCardSecurityNumber,
      creditCardExpirationDate: this.creditCardExpirationDate,
      paymentCurrency: this.paymentCurrency
    });

    this.currency = 'EUR';
    this.orderPlacedText = '';
  }

  loadAll() {
    this.checkoutService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<ICheckout[]>) => this.paginateCheckouts(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  reset() {
    this.page = 0;
    this.products = [];
    // this.loadAll();
  }

  loadPage(page) {
    this.page = page;
    // this.loadAll();
  }

  ngOnInit() {
    this.getProductsInCart();
    // this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    // this.registerChangeInCheckouts();
    // this.onError(this.orderForm.get('firstName').value);
  }

  ngOnDestroy() {
    // this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICheckout) {
    return item.id;
  }

  registerChangeInCheckouts() {
    this.eventSubscriber = this.eventManager.subscribe('checkoutListModification', response => this.reset());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateCheckouts(data: ICheckout[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    for (let i = 0; i < data.length; i++) {
      this.checkouts.push(data[i]);
    }
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  getProductsInCart() {
    this.cartService.query().subscribe(
      (res: HttpResponse<ICart[]>) => {
        this.carts = res.body;

        if (this.carts.length !== 0) {
          this.activeCartId = this.carts[0].id;
          this.getProductsWithCartId();
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  getProductsWithCartId() {
    this.checkoutService.goToCheckout(this.activeCartId).subscribe(
      (result: IProduct[]) => {
        this.products = result;
      },
      (result: HttpErrorResponse) => this.onError(result.message)
    );
  }

  trackProductId(index: number, item: IProduct) {
    return item.id;
  }

  clearCart() {
    this.checkoutService.emptyCart(this.activeCartId).subscribe(() => {
      this.getProductsInCart();
    });
  }

  placeOrder() {
    if (
      this.orderForm.get('lastName').value === '' ||
      this.orderForm.get('eMailAddress').value === '' ||
      this.orderForm.get('address').value === '' ||
      this.orderForm.get('creditCardNumber').value === null ||
      this.orderForm.get('creditCardSecurityNumber').value === null ||
      this.orderForm.get('creditCardExpirationDate').value === null ||
      (this.orderForm.get('paymentCurrency').value !== 'EUR' && this.orderForm.get('paymentCurrency').value !== 'USD')
    ) {
      this.onError('Enter valid customer information!');
    } else {
      this.checkoutPerson = new CheckoutPerson(
        this.orderForm.get('firstName').value,
        this.orderForm.get('lastName').value,
        this.orderForm.get('eMailAddress').value,
        this.orderForm.get('address').value,
        this.orderForm.get('creditCardNumber').value,
        this.orderForm.get('creditCardSecurityNumber').value,
        this.orderForm.get('creditCardExpirationDate').value
      );

      this.placeOrderInformation = new PlaceOrderInformation(
        this.activeCartId,
        this.orderForm.get('paymentCurrency').value,
        this.checkoutPerson
      );

      this.checkoutService.placeOrder(this.placeOrderInformation).subscribe(
        (response: string) => {
          this.orderPlacedText = response;
          this.clearCart();
        },
        (response: HttpErrorResponse) => {
          this.onError('Error response: ' + response.message);
          this.onError('Resend the order');
        }
      );
    }
  }
}
