import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'cart',
        loadChildren: () => import('./cart/cart/cart.module').then(m => m.CartCartModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./productCatalogue/product/product.module').then(m => m.ProductCatalogueProductModule)
      },
      {
        path: 'checkout',
        loadChildren: () => import('./checkout/checkout/checkout.module').then(m => m.CheckoutCheckoutModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FrontendEntityModule {}
