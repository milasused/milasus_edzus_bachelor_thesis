import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProduct } from 'app/shared/model/productCatalogue/product.model';
import { ICart } from 'app/shared/model/cart/cart.model';
import { ICartProduct } from 'app/shared/model/cart/cartProduct.model';

@Injectable({ providedIn: 'root' })
export class ProductCartService {
  public resourceUrl = SERVER_API_URL + 'services/cart/api/carts/addProduct';
  // addProduct URL: addProduct/[cartId]

  constructor(protected http: HttpClient) {}

  addProduct(cartProduct: ICartProduct, cartId: string): Observable<HttpResponse<any>> {
    return this.http.post<ICartProduct>(`${this.resourceUrl}/${cartId}`, cartProduct, { observe: 'response' });
  }

  getAllProducts(cartId: string) {
    return this.http.get(`${this.resourceUrl}/getCartProducts/${cartId}`);
  }
}
