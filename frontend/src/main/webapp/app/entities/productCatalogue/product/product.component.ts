import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IProduct } from 'app/shared/model/productCatalogue/product.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ProductService } from './product.service';

import { ProductCartService } from './productCart.service';
import { ICartProduct, CartProduct } from 'app/shared/model/cart/cartProduct.model';

import { CartService } from 'app/entities/cart/cart/cart.service';
import { ICart, Cart } from 'app/shared/model/cart/cart.model';

@Component({
  selector: 'jhi-product',
  templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit, OnDestroy {
  currentAccount: any;
  products: IProduct[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  cartProduct: ICartProduct;
  responseFromCart: any;
  activeCartId: string;
  carts: ICart[];
  cart: ICart;
  requestCounter: number;

  constructor(
    protected productService: ProductService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected productCartService: ProductCartService,
    protected cartService: CartService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.activeCartId = '';
    });
  }

  loadAll() {
    this.productService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IProduct[]>) => this.paginateProducts(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/product'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/product',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInProducts();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IProduct) {
    return item.id;
  }

  registerChangeInProducts() {
    this.eventSubscriber = this.eventManager.subscribe('productListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateProducts(data: IProduct[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.products = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  addProductToCartCall(productId: string, productPrice: number) {
    if (this.activeCartId === '') {
      this.onError('Retry adding Product to cart');
    } else {
      this.cartProduct = new CartProduct(productId, productPrice);
      this.onError('Product added to cart');
      this.productCartService.addProduct(this.cartProduct, this.activeCartId).subscribe();
    }
  }

  addProductToCart(productId: string, productPrice: number) {
    this.cartService
      .query()
      .pipe(
        filter((res: HttpResponse<ICart[]>) => res.ok),
        map((res: HttpResponse<ICart[]>) => res.body)
      )
      .subscribe(
        (res: ICart[]) => {
          this.carts = res;

          if (this.carts.length === 0) {
            this.cart = new Cart();
            this.cartService.create(this.cart).subscribe();
          } else {
            this.activeCartId = this.carts[0].id;
          }

          this.addProductToCartCall(productId, productPrice);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  deleteAllCarts() {
    // this.getAllCarts();
    for (this.requestCounter = 0; this.requestCounter < this.carts.length; this.requestCounter += 1) {
      this.cartService.delete(this.carts[this.requestCounter].id).subscribe();
    }
  }
}
