package com.ms.request.web.rest;

import com.ms.request.domain.Request;
import com.ms.request.repository.RequestRepository;
import com.ms.request.repository.search.RequestSearchRepository;
import com.ms.request.security.jwt.TokenProvider;
import com.ms.request.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.ms.request.security.jwt.TokenProvider;
import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.*;

/**
 * REST controller for managing {@link com.ms.request.domain.Request}.
 */
@RestController
@RequestMapping("/api")
public class RequestResource {

    private final Logger log = LoggerFactory.getLogger(RequestResource.class);

    private static final String ENTITY_NAME = "requestRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RequestRepository requestRepository;

    private final RequestSearchRepository requestSearchRepository;

    public RequestResource(RequestRepository requestRepository, RequestSearchRepository requestSearchRepository) {
        this.requestRepository = requestRepository;
        this.requestSearchRepository = requestSearchRepository;
    }

    /**
     * {@code POST  /requests} : Create a new request.
     *
     * @param request the request to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new request, or with status {@code 400 (Bad Request)} if the request has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/requests")
    public ResponseEntity<Request> createRequest(@RequestBody Request request) throws URISyntaxException {
        log.debug("REST request to save Request : {}", request);
        if (request.getId() != null) {
            throw new BadRequestAlertException("A new request cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Request result = requestRepository.save(request);
        requestSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /requests} : Updates an existing request.
     *
     * @param request the request to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated request,
     * or with status {@code 400 (Bad Request)} if the request is not valid,
     * or with status {@code 500 (Internal Server Error)} if the request couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/requests")
    public ResponseEntity<Request> updateRequest(@RequestBody Request request) throws URISyntaxException {
        log.debug("REST request to update Request : {}", request);
        if (request.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Request result = requestRepository.save(request);
        requestSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, request.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /requests} : get all the requests.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requests in body.
     */
    @GetMapping("/requests")
    public List<Request> getAllRequests() {
        try {

            String secretKey =  "my-secret-key-which-should-be-changed-in-production-and-be-base64-encoded";



            URL url = new URL("http://localhost:8080/services/response/api/response/test");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            long now = (new Date()).getTime();
            Date validity;
            validity = new Date(now + 1000000);

            String jwtToken = Jwts.builder()
                .setSubject("admin")
                .claim("auth", "ROLE_ADMIN,ROLE_USER")
                .setExpiration(validity)
                .signWith(
                    SignatureAlgorithm.HS512,
                    secretKey.getBytes("UTF-8")
                )
                .compact();

            //System.out.println("Created token: " + jwtToken);
            //System.out.println("Token from frontend: " + "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU3NTE5NzM0N30.T6FR39CqB1sq3xjJm9e6ltLC74TSCyqRGLZZ-ZVx4lDA2qB24ylt8YnneQjrctS9QvnDRcd7A1l5kBNFbiyUaw");

            conn.setRequestProperty("Authorization", "Bearer " + jwtToken);
            //e.g. bearer token= eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "text/plain");
            conn.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;

            StringBuffer response = new StringBuffer();
            while ((output = in.readLine()) != null) {
                response.append(output);
            }

            System.out.println("Response:-" + response.toString());

            in.close();

        }
        catch(Exception e){
            log.error("Communication exception: " + e.getMessage());
        }

        log.debug("REST request to get all Requests");
        return requestRepository.findAll();
    }

    /**
     * {@code GET  /requests/:id} : get the "id" request.
     *
     * @param id the id of the request to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the request, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/requests/{id}")
    public ResponseEntity<Request> getRequest(@PathVariable Long id) {
        log.debug("REST request to get Request : {}", id);
        Optional<Request> request = requestRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(request);
    }

    /**
     * {@code DELETE  /requests/:id} : delete the "id" request.
     *
     * @param id the id of the request to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/requests/{id}")
    public ResponseEntity<Void> deleteRequest(@PathVariable Long id) {
        log.debug("REST request to delete Request : {}", id);
        requestRepository.deleteById(id);
        requestSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/requests?query=:query} : search for the request corresponding
     * to the query.
     *
     * @param query the query of the request search.
     * @return the result of the search.
     */
    @GetMapping("/_search/requests")
    public List<Request> searchRequests(@RequestParam String query) {
        log.debug("REST request to search Requests for query {}", query);
        return StreamSupport
            .stream(requestSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
