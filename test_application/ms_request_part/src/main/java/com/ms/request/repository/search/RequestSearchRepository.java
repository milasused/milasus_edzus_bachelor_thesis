package com.ms.request.repository.search;

import com.ms.request.domain.Request;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Request} entity.
 */
public interface RequestSearchRepository extends ElasticsearchRepository<Request, Long> {
}
