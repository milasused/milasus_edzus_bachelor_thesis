/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ms.response.web.rest.vm;
