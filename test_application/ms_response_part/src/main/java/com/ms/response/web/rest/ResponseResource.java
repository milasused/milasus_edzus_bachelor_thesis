package com.ms.response.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * ResponseResource controller
 */
@RestController
@RequestMapping("/api/response")
public class ResponseResource {


    private final Logger log = LoggerFactory.getLogger(ResponseResource.class);

    /**
    * GET test
    */
    @GetMapping("/test")
    public String test() {
        return "test";
    }

}
