package com.ms.response.web.rest;

import com.ms.response.ResponseApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the ResponseResource REST controller.
 *
 * @see ResponseResource
 */
@SpringBootTest(classes = ResponseApp.class)
public class ResponseResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        ResponseResource responseResource = new ResponseResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(responseResource)
            .build();
    }

    /**
     * Test test
     */
    @Test
    public void testTest() throws Exception {
        restMockMvc.perform(get("/api/response/test"))
            .andExpect(status().isOk());
    }
}
