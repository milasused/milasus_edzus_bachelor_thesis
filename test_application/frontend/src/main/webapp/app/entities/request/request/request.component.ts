import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRequest } from 'app/shared/model/request/request.model';
import { AccountService } from 'app/core';
import { RequestService } from './request.service';

@Component({
  selector: 'jhi-request',
  templateUrl: './request.component.html'
})
export class RequestComponent implements OnInit, OnDestroy {
  requests: IRequest[];
  currentAccount: any;
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(
    protected requestService: RequestService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.requestService
        .search({
          query: this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<IRequest[]>) => res.ok),
          map((res: HttpResponse<IRequest[]>) => res.body)
        )
        .subscribe((res: IRequest[]) => (this.requests = res), (res: HttpErrorResponse) => this.onError(res.message));
      return;
    }
    this.requestService
      .query()
      .pipe(
        filter((res: HttpResponse<IRequest[]>) => res.ok),
        map((res: HttpResponse<IRequest[]>) => res.body)
      )
      .subscribe(
        (res: IRequest[]) => {
          this.requests = res;
          this.currentSearch = '';
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInRequests();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRequest) {
    return item.id;
  }

  registerChangeInRequests() {
    this.eventSubscriber = this.eventManager.subscribe('requestListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
