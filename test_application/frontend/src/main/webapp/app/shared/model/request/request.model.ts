export interface IRequest {
  id?: number;
}

export class Request implements IRequest {
  constructor(public id?: number) {}
}
