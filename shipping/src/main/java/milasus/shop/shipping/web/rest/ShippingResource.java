package milasus.shop.shipping.web.rest;

import milasus.shop.shipping.domain.Shipping;
import milasus.shop.shipping.exception.ShippingPersonRuntimeException;
import milasus.shop.shipping.exception.ShippingProductsRuntimeException;
import milasus.shop.shipping.repository.ShippingRepository;
import milasus.shop.shipping.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import person.Person;
import personProduct.PersonProductObject;
import product.Product;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link milasus.shop.shipping.domain.Shipping}.
 */
@RestController
@RequestMapping("/api/shipping")
public class ShippingResource {

    private final Logger log = LoggerFactory.getLogger(ShippingResource.class);

    private static final String ENTITY_NAME = "shippingShipping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShippingRepository shippingRepository;

    public ShippingResource(ShippingRepository shippingRepository) {
        this.shippingRepository = shippingRepository;
    }

    /**
     * {@code GET  /shippings} : get all the shippings.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shippings in body.
     */
    @GetMapping("/shippings")
    public List<Shipping> getAllShippings() {
        log.debug("REST request to get all Shippings");
        return shippingRepository.findAll();
    }

    /**
     * {@code GET  /shippings/:id} : get the "id" shipping.
     *
     * @param id the id of the shipping to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shipping, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shippings/{id}")
    public ResponseEntity<Shipping> getShipping(@PathVariable String id) {
        log.debug("REST request to get Shipping : {}", id);
        Optional<Shipping> shipping = shippingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(shipping);
    }

    /**
     * {@code DELETE  /shippings/:id} : delete the "id" shipping.
     *
     * @param id the id of the shipping to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shippings/{id}")
    public ResponseEntity<Void> deleteShipping(@PathVariable String id) {
        log.debug("REST request to delete Shipping : {}", id);
        shippingRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }




    /**
     * POST productShipping
     */
    @PostMapping("/product-shipping")
    public String productShipping(@RequestBody PersonProductObject personProductObject) {

        this.checkPerson(personProductObject.getPerson());

        this.checkProductList(personProductObject.getProductList());

        log.debug("REST request to save Shipping : {}", personProductObject);

        Shipping productShippingToSave = new Shipping ();

        productShippingToSave.setCustomerAddress(personProductObject.getPerson().getAddress());
        productShippingToSave.setCustomerLastName(personProductObject.getPerson().getLastName());
        productShippingToSave.setShippedProducts(personProductObject.getProductList());

        Shipping result = shippingRepository.save(productShippingToSave);

        return "Shipping products";
    }

    /**
     * POST calculateShippingCosts depending on land and product cost
     */
    @PostMapping("/calculate-shipping-costs")
    public Integer calculateShippingCosts(@RequestBody PersonProductObject personProductObject) {

        this.checkPerson(personProductObject.getPerson());

        this.checkProductList(personProductObject.getProductList());

        return 1000;
    }

    private void checkProductList(List<Product> productList) {
        if(productList.isEmpty() == true){
            throw new ShippingProductsRuntimeException("Product List is empty");
        }

        for(Product product: productList){
            this.checkProduct(product);
        }
    }

    private void checkProduct(Product product) {
        if(product.getId() == null){
            throw new ShippingProductsRuntimeException("Product List contains invalid values");
        }
        if(product.getPrice() == null || product.getPrice() < 0){
            throw new ShippingProductsRuntimeException("Product List contains invalid product " +
                "price");
        }
    }

    private void checkPerson(Person person) {
        if(person.getAddress() == null || person.getFirstName() == null ||person.getLastName() == null){
            throw new ShippingPersonRuntimeException("Person is not valid");
        }
    }

}
