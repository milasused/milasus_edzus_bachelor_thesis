/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.shipping.web.rest.vm;
