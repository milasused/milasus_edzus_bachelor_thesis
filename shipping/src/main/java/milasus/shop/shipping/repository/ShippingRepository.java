package milasus.shop.shipping.repository;

import milasus.shop.shipping.domain.Shipping;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Shipping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShippingRepository extends MongoRepository<Shipping, String> {

}
