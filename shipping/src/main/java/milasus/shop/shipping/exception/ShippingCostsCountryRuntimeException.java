package milasus.shop.shipping.exception;

public class ShippingCostsCountryRuntimeException extends RuntimeException {

    public ShippingCostsCountryRuntimeException(String errorMessage){
        super(errorMessage);
    }

}
