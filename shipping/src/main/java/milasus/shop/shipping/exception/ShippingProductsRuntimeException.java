package milasus.shop.shipping.exception;

/**
 * Throws Exceptions in connection to Products passed to Shipping Service
 */
public class ShippingProductsRuntimeException extends RuntimeException {
    public ShippingProductsRuntimeException(String errorMessage){
        super(errorMessage);
    }
}
