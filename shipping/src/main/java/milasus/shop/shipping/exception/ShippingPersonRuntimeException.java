package milasus.shop.shipping.exception;

/**
 * Throwing exceptions in connection to invalid Person data sent to Shipping Service
 */
public class ShippingPersonRuntimeException extends RuntimeException {
    public ShippingPersonRuntimeException(String errorMessage){
        super(errorMessage);
    }
}
