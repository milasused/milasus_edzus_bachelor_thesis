package milasus.shop.shipping.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import product.Product;

import java.io.Serializable;
import java.util.List;

/**
 * A Shipping.
 */
@Document(collection = "shipping")
public class Shipping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("customer_last_name")
    private String customerLastName;

    @Field("customer_address")
    private String customerAddress;

    @Field("shipped_products")
    private List<Product> shippedProducts;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public Shipping customerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
        return this;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public Shipping customerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
        return this;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public List<Product> getShippedProducts() {
        return shippedProducts;
    }

    public Shipping shippingProducts(List<Product> productList){
        this.shippedProducts = productList;
        return this;
    }

    public void setShippedProducts(List<Product> shippedProducts) {
        this.shippedProducts = shippedProducts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shipping)) {
            return false;
        }
        return id != null && id.equals(((Shipping) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Shipping{" +
            "id=" + getId() +
            ", customerLastName='" + getCustomerLastName() + "'" +
            ", customerAddress='" + getCustomerAddress() + "'" +
            ", shippedProducts='" + getShippedProducts().toString() + "'" +
            "}";
    }


}
