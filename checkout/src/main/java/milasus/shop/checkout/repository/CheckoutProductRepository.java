package milasus.shop.checkout.repository;

import milasus.shop.checkout.domain.CheckoutProduct;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the CheckoutProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CheckoutProductRepository extends MongoRepository<CheckoutProduct, String> {

}
