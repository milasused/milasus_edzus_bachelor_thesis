/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.checkout.web.rest.vm;
