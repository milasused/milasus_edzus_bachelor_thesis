package milasus.shop.checkout.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import milasus.shop.checkout.domain.Checkout;
import milasus.shop.checkout.orchestrator.Orchestrator;
import milasus.shop.checkout.repository.CheckoutRepository;
import milasus.shop.checkout.web.rest.errors.BadRequestAlertException;
import orderInformation.PlaceOrderInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link milasus.shop.checkout.domain.Checkout}.
 */
@RestController
@RequestMapping("/api")
public class CheckoutResource {

    private final Logger log = LoggerFactory.getLogger(CheckoutResource.class);

    private static final String ENTITY_NAME = "checkoutCheckout";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CheckoutRepository checkoutRepository;

    private Orchestrator orchestrator;

    public CheckoutResource(CheckoutRepository checkoutRepository) {
        this.checkoutRepository = checkoutRepository;
        this.orchestrator = new Orchestrator();
    }

    /**
     * {@code POST  /checkouts} : Create a new checkout.
     *
     * @param checkout the checkout to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new checkout, or with status {@code 400 (Bad Request)} if the checkout has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/checkouts")
    public ResponseEntity<Checkout> createCheckout(@Valid @RequestBody Checkout checkout) throws URISyntaxException {
        log.debug("REST request to save Checkout : {}", checkout);
        if (checkout.getId() != null) {
            throw new BadRequestAlertException("A new checkout cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Checkout result = checkoutRepository.save(checkout);
        return ResponseEntity.created(new URI("/api/checkouts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /checkouts} : Updates an existing checkout.
     *
     * @param checkout the checkout to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated checkout,
     * or with status {@code 400 (Bad Request)} if the checkout is not valid,
     * or with status {@code 500 (Internal Server Error)} if the checkout couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/checkouts")
    public ResponseEntity<Checkout> updateCheckout(@Valid @RequestBody Checkout checkout) throws URISyntaxException {
        log.debug("REST request to update Checkout : {}", checkout);
        if (checkout.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Checkout result = checkoutRepository.save(checkout);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, checkout.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /checkouts} : get all the checkouts.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of checkouts in body.
     */
    @GetMapping("/checkouts")
    public ResponseEntity<List<Checkout>> getAllCheckouts(Pageable pageable) {
        log.debug("REST request to get a page of Checkouts");
        Page<Checkout> page = checkoutRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /checkouts/:id} : get the "id" checkout.
     *
     * @param id the id of the checkout to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the checkout, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/checkouts/{id}")
    public ResponseEntity<Checkout> getCheckout(@PathVariable String id) {
        log.debug("REST request to get Checkout : {}", id);
        Optional<Checkout> checkout = checkoutRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(checkout);
    }

    /**
     * {@code DELETE  /checkouts/:id} : delete the "id" checkout.
     *
     * @param id the id of the checkout to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/checkouts/{id}")
    public ResponseEntity<Void> deleteCheckout(@PathVariable String id) {
        log.debug("REST request to delete Checkout : {}", id);
        checkoutRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PostMapping("/go-to-checkout")
    public String goToCheckout(@RequestBody String cartId) {

        String productInformationList = this.orchestrator.getProductInformationString(cartId);

        return productInformationList;
    }

    @PostMapping("/place-order")
    public String placeOrder(@RequestBody PlaceOrderInformation placeOrderInformation){

        Checkout checkout = new Checkout ();

        checkout.setLastName(placeOrderInformation.getCheckoutPerson().getLastName());
        checkout.setFirstName(placeOrderInformation.getCheckoutPerson().getFirstName());
        checkout.setCreditCardExpirationDate(placeOrderInformation.getCheckoutPerson().getCreditCardExpirationDate());
        checkout.setCreditCardNumber(placeOrderInformation.getCheckoutPerson().getCreditCardNumber().toString());
        checkout.setCreditCardSecurityNumber(placeOrderInformation.getCheckoutPerson().getCreditCardSecurityNumber());
        checkout.seteMail(placeOrderInformation.getCheckoutPerson().geteMailAddress());
        checkout.setAddress(placeOrderInformation.getCheckoutPerson().getAddress());

        this.checkoutRepository.save(checkout);

        return this.orchestrator.finalizeCheckout(placeOrderInformation);
    }

    @DeleteMapping("/empty-cart/{cartId}")
    public void emptyCart(@PathVariable String cartId){

        this.orchestrator.emptyCart(cartId);
    }


    /*
    @PostMapping("/test/currency")
    public String testCurrency(@RequestBody List<Product> productList){

        return this.orchestrator.testCurrencyCommunicator(productList).toString();
    }

    @PostMapping("/test/shipping-costs")
    public String testShippingCosts(@RequestBody PersonProductObject personProductObject){

        return this.orchestrator.testShippingCosts(personProductObject);
    }

    @PostMapping("/test/product-shipping")
    public String testProductShipping(@RequestBody PersonProductObject personProductObject){

        return this.orchestrator.testProductShipping(personProductObject);
    }

    @PostMapping("/test/send-email")
    public String testEmail(@RequestBody Person person){

        return this.orchestrator.testEmail(person);
    }

    @PostMapping("/test/new-payment")
    public String testEmail(@RequestBody PersonPaymentObject personPaymentObject){

        return this.orchestrator.testPayment(personPaymentObject);
    }

    @PostMapping("test/product-catalogue-products")
    public String testProductCatalogue(@RequestBody List<String> productIds){
        log.info("product IDs toString: " + productIds.toString());
        return this.orchestrator.testProductCatalogue(productIds);
    }
    */
}
