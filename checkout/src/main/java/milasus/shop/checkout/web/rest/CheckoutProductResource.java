package milasus.shop.checkout.web.rest;

import milasus.shop.checkout.domain.CheckoutProduct;
import milasus.shop.checkout.repository.CheckoutProductRepository;
import milasus.shop.checkout.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link milasus.shop.checkout.domain.CheckoutProduct}.
 */
@RestController
@RequestMapping("/api")
public class CheckoutProductResource {

    private final Logger log = LoggerFactory.getLogger(CheckoutProductResource.class);

    private static final String ENTITY_NAME = "checkoutCheckoutProduct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CheckoutProductRepository checkoutProductRepository;

    public CheckoutProductResource(CheckoutProductRepository checkoutProductRepository) {
        this.checkoutProductRepository = checkoutProductRepository;
    }

    /**
     * {@code POST  /checkout-products} : Create a new checkoutProduct.
     *
     * @param checkoutProduct the checkoutProduct to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new checkoutProduct, or with status {@code 400 (Bad Request)} if the checkoutProduct has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/checkout-products")
    public ResponseEntity<CheckoutProduct> createCheckoutProduct(@RequestBody CheckoutProduct checkoutProduct) throws URISyntaxException {
        log.debug("REST request to save CheckoutProduct : {}", checkoutProduct);
        if (checkoutProduct.getId() != null) {
            throw new BadRequestAlertException("A new checkoutProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CheckoutProduct result = checkoutProductRepository.save(checkoutProduct);
        return ResponseEntity.created(new URI("/api/checkout-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /checkout-products} : Updates an existing checkoutProduct.
     *
     * @param checkoutProduct the checkoutProduct to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated checkoutProduct,
     * or with status {@code 400 (Bad Request)} if the checkoutProduct is not valid,
     * or with status {@code 500 (Internal Server Error)} if the checkoutProduct couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/checkout-products")
    public ResponseEntity<CheckoutProduct> updateCheckoutProduct(@RequestBody CheckoutProduct checkoutProduct) throws URISyntaxException {
        log.debug("REST request to update CheckoutProduct : {}", checkoutProduct);
        if (checkoutProduct.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CheckoutProduct result = checkoutProductRepository.save(checkoutProduct);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, checkoutProduct.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /checkout-products} : get all the checkoutProducts.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of checkoutProducts in body.
     */
    @GetMapping("/checkout-products")
    public List<CheckoutProduct> getAllCheckoutProducts() {
        log.debug("REST request to get all CheckoutProducts");
        return checkoutProductRepository.findAll();
    }

    /**
     * {@code GET  /checkout-products/:id} : get the "id" checkoutProduct.
     *
     * @param id the id of the checkoutProduct to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the checkoutProduct, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/checkout-products/{id}")
    public ResponseEntity<CheckoutProduct> getCheckoutProduct(@PathVariable String id) {
        log.debug("REST request to get CheckoutProduct : {}", id);
        Optional<CheckoutProduct> checkoutProduct = checkoutProductRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(checkoutProduct);
    }

    /**
     * {@code DELETE  /checkout-products/:id} : delete the "id" checkoutProduct.
     *
     * @param id the id of the checkoutProduct to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/checkout-products/{id}")
    public ResponseEntity<Void> deleteCheckoutProduct(@PathVariable String id) {
        log.debug("REST request to delete CheckoutProduct : {}", id);
        checkoutProductRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
