package milasus.shop.checkout.communicator;

import org.json.JSONArray;

/**
 * Manage the communication with cart service
 */
public class CartCommunicator extends DefaultCommunicator{

    public CartCommunicator() {
        super();
        this.setServiceEndpoint(this.getServiceEndpoint() + "/services/cart/api/carts");
    }

    public String requestProducts(String cartID){

        return this.sendRequest("/getCartProducts/" + cartID, "GET", null);
    }

    public void removeProducts(JSONArray productsToRemove, String cartId){
        this.sendRequest("/remove-products/" + cartId, "DELETE",
            productsToRemove.toString());
    }


}
