package milasus.shop.checkout.communicator;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Base class for managing communication between the services
 *
 */
public abstract class DefaultCommunicator {

    private String serviceEndpoint;
    private String secretKey;

    public DefaultCommunicator() {
        this.serviceEndpoint = "http://localhost:8080";
        this.secretKey = "my-secret-key-which-should-be-changed-in-production-and-be-base64-encoded";
    }

    protected String getServiceEndpoint() {
        return serviceEndpoint;
    }

    protected void setServiceEndpoint(String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /**
     * Default request to another Service.
     * Precise endpoint received from Communicator, parsing happens in implemented Communicator
     * @param endpointRoute precise endpoint routing
     * @param requestMethod GET, POST, PUT etc.
     * @return String element which is the response (can be parsed to JSONArray, JSONObject etc.)
     */
    protected String sendRequest(String endpointRoute, String requestMethod, String body){

        StringBuffer response = new StringBuffer();

        try{
            URL url = new URL(this.serviceEndpoint + endpointRoute);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            long now = (new Date()).getTime();
            Date validity;
            validity = new Date(now + 1000000);

            String jwtToken = Jwts.builder()
                .setSubject("admin")
                .claim("auth", "ROLE_ADMIN,ROLE_USER")
                .setExpiration(validity)
                .signWith(
                    SignatureAlgorithm.HS512,
                    secretKey .getBytes("UTF-8")
                )
                .compact();

            conn.setRequestProperty("Authorization", "Bearer " + jwtToken);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod(requestMethod);

            if(body != null) { //Reference: https://stackoverflow.com/questions/44305351/send-data-in-request-body-using-httpurlconnection
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                osw.write(body);
                osw.flush();
                osw.close();
                os.close();
            }

            conn.connect();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;

            while ((output = in.readLine()) != null) {
                response.append(output);
            }

            in.close();

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response.toString();
    }


    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
