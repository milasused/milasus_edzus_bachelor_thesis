package milasus.shop.checkout.communicator;

import org.json.JSONObject;

/**
 * Manage the communication with shipping service
 */
public class ShippingCommunicator extends DefaultCommunicator{
    public ShippingCommunicator() {
        super();
        this.setServiceEndpoint(this.getServiceEndpoint() + "/services/shipping/api/shipping");
    }



    public String getShippingPrice(JSONObject personProductObject) {
        return this.sendRequest("/calculate-shipping-costs", "POST",
            personProductObject.toString());
    }

    public String shipProducts(JSONObject personProductObject) {
        return this.sendRequest("/product-shipping", "POST", personProductObject.toString());
    }
}
