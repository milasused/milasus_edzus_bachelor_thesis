package milasus.shop.checkout.communicator;

import org.json.JSONArray;

/**
 * Manage the communication with product catalogue service
 */

public class ProductCatalogueCommunicator extends DefaultCommunicator{

    public ProductCatalogueCommunicator() {
        super();
        this.setServiceEndpoint(this.getServiceEndpoint() + "/services/productcatalogue/api");
    }

    /**
     * get Product prices for all products in a List (= Cart)
     * @param productIdList
     * @return
     */
    public String getProductsFromCatalogue (JSONArray productIdList) {

        return this.sendRequest("/product-catalogue-products", "POST", productIdList.toString());
    }

}
