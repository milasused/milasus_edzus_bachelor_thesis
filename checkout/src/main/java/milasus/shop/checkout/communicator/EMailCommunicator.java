package milasus.shop.checkout.communicator;

import org.json.JSONObject;

/**
 * Manage the communication with EMail service
 */
public class EMailCommunicator extends DefaultCommunicator{
    public EMailCommunicator() {
        super();
        this.setServiceEndpoint(this.getServiceEndpoint() + "/services/email/api/email");
    }



    public String sendEMail(JSONObject checkoutPerson) {

        return this.sendRequest("/send-email", "POST", checkoutPerson.toString());

    }
}
