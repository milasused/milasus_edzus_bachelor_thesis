package milasus.shop.checkout.communicator;

import org.json.JSONObject;

/**
 * Manage the communication with payment service
 */
public class PaymentCommunicator extends DefaultCommunicator {
    public PaymentCommunicator() {
        super();
        this.setServiceEndpoint(this.getServiceEndpoint() + "/services/payment/api/payment");
    }

    public String chargeClient(JSONObject personPaymentObject) {
        return this.sendRequest("/new-payment", "POST", personPaymentObject.toString());
    }
}
