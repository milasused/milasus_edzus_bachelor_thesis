package milasus.shop.checkout.communicator;

import org.json.JSONArray;

/**
 * Manage the communication with Currency service
 */
public class CurrencyCommunicator extends DefaultCommunicator{
    public CurrencyCommunicator() {
        super();
        this.setServiceEndpoint(this.getServiceEndpoint() + "/services/currency/api/currency");
    }

    public String calculatePrices(String otherCurrency, JSONArray productList) {
        return this.sendRequest("/calculate-other-currency-product-list/" + otherCurrency, "POST"
            , productList.toString());
    }


}
