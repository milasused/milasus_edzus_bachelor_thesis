package milasus.shop.checkout;

/**
 * Local Person object to represet a person
 * TODO: insert validity checks for EMail address, Credit cart etc.
 */
public class CheckoutPerson {
    String firstName;
    String lastName;
    String address;
    String eMailAddress;
    Integer creditCardNumber;
    Integer creditCardSecurityNumber;
    Integer creditCardExpirationDate;

    public CheckoutPerson(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String geteMailAddress() {
        return eMailAddress;
    }

    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }

    public Integer getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(Integer creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public Integer getCreditCardSecurityNumber() {
        return creditCardSecurityNumber;
    }

    public void setCreditCardSecurityNumber(Integer creditCardSecurityNumber) {
        this.creditCardSecurityNumber = creditCardSecurityNumber;
    }

    public Integer getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    public void setCreditCardExpirationDate(Integer creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
    }
}
