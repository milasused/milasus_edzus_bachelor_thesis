package milasus.shop.checkout.orchestrator;

import org.json.JSONArray;
import org.json.JSONObject;
import person.Person;
import personPayment.PersonPaymentObject;
import personProduct.PersonProductObject;
import product.Product;
import productCatalogue.ProductCatalogueProduct;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public JsonParser() {
    }

    public JSONObject parse(String stringToParse){
        return new JSONObject(stringToParse);
    }

    public JSONArray parseArray (String stringToParse){
        //System.out.println("String to Parse: " + stringToParse);
        return new JSONArray(stringToParse);
    }

    public JSONArray createProductArray (List<Product> productList){
        JSONArray jsonArray = new JSONArray();

        for(int i = 0; i < productList.size(); ++i){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", productList.get(i).getId());
            jsonObject.put("price", productList.get(i).getPrice());
            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    public JSONObject createPerson (Person person){
        JSONObject jsonPerson = new JSONObject();

        jsonPerson.put("address", person.getAddress());
        jsonPerson.put("creditCardExpirationDate", person.getCreditCardExpirationDate());
        jsonPerson.put("creditCardNumber", person.getCreditCardNumber());
        jsonPerson.put("creditCardSecurityNumber", person.getCreditCardSecurityNumber());
        jsonPerson.put("eMailAddress", person.geteMailAddress());
        jsonPerson.put("firstName", person.getFirstName());
        jsonPerson.put("lastName", person.getLastName());

        return jsonPerson;
    }

    public JSONObject transferPersonProductToJsonObject(PersonProductObject personProductObject){
        JSONObject jsonPerson = this.createPerson(personProductObject.getPerson());

        JSONArray jsonProductList =
            this.createProductArray(personProductObject.getProductList());

        JSONObject jsonPersonProductObject = new JSONObject();

        jsonPersonProductObject.put("person", jsonPerson);
        jsonPersonProductObject.put("productList", jsonProductList);

        return jsonPersonProductObject;
    }

    public JSONObject parsePersonPaymentObject(PersonPaymentObject personPaymentObject){

        JSONObject jsonPerson = this.createPerson(personPaymentObject.getPerson());

        JSONObject jsonPersonPaymentObject = new JSONObject();

        jsonPersonPaymentObject.put("person", jsonPerson);
        jsonPersonPaymentObject.put("amountToPay", personPaymentObject.getAmountToPay());

        return jsonPersonPaymentObject;
    }

    public JSONArray createProductIdList(JSONArray productsInCart){
        List<String> productsInCartIds = new ArrayList<String>();

        for(int i = 0; i < productsInCart.length(); ++i){
            JSONObject jsonProduct = new JSONObject(productsInCart.get(i).toString());
            productsInCartIds.add( (String) jsonProduct.get("id"));
        }

        return this.parseArray(productsInCartIds.toString());
    }

    public List<Product> parseJSONArrayToProductList(JSONArray productsInCart){
        List<Product> returnProductList = new ArrayList<>();

        for(int i = 0; i < productsInCart.length(); ++i){
            JSONObject jsonProduct = this.parse(productsInCart.get(i).toString());
            Product newProduct = new Product (
                jsonProduct.get("id").toString(),
                Integer.parseInt(jsonProduct.get(
                "price").toString()));
            returnProductList.add(newProduct);
        }

        return returnProductList;
    }

    public List<Product> parseStringToProductList(String productInformation){
        JSONArray productInformationArray = this.parseArray(productInformation);

        return this.parseJSONArrayToProductList(productInformationArray);
    }

    public List<ProductCatalogueProduct> parseJSONArrayToProductCatalogueProductList(String productsFromProductCatalogue) {

        JSONArray jsonArrayProducts = this.parseArray(productsFromProductCatalogue);

        List<ProductCatalogueProduct> returnProductList = new ArrayList<>();

        for(int i = 0; i < jsonArrayProducts.length(); ++i){
            JSONObject jsonProduct = this.parse(jsonArrayProducts.get(i).toString());

            ProductCatalogueProduct newProduct = new ProductCatalogueProduct (
                jsonProduct.get("id").toString(),
                jsonProduct.get("name").toString(),
                Integer.parseInt(jsonProduct.get("price").toString()),
                jsonProduct.get("description").toString(),
                jsonProduct.get("fotoPath").toString()
                );
            returnProductList.add(newProduct);
        }

        return returnProductList;

    }
}
