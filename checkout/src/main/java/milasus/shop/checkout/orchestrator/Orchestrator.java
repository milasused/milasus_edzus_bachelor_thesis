package milasus.shop.checkout.orchestrator;

import milasus.shop.checkout.communicator.*;
import orderInformation.PlaceOrderInformation;
import org.json.JSONArray;
import personPayment.PersonPaymentObject;
import personProduct.PersonProductObject;
import product.Product;
import productCatalogue.ProductCatalogueProduct;

import java.util.List;

// TODO: implement Exception management
public class Orchestrator {

    CartCommunicator cartCommunicator;
    CurrencyCommunicator currencyCommunicator;
    EMailCommunicator eMailCommunicator;
    PaymentCommunicator paymentCommunicator;
    ProductCatalogueCommunicator productCatalogueCommunicator;
    ShippingCommunicator shippingCommunicator;
    JsonParser jsonParser;

    public Orchestrator (){
        this.cartCommunicator = new CartCommunicator();
        this.currencyCommunicator = new CurrencyCommunicator();
        this.eMailCommunicator = new EMailCommunicator();
        this.paymentCommunicator = new PaymentCommunicator();
        this.productCatalogueCommunicator = new ProductCatalogueCommunicator();
        this.shippingCommunicator = new ShippingCommunicator();
        this.jsonParser = new JsonParser();
    }

    /**
     * receive all product IDs, which are in a Cart with cartID
     * @return
     * @param cartId
     */
    private JSONArray getCartProductID(String cartId) {
        return this.jsonParser.parseArray(this.cartCommunicator.requestProducts(cartId));
    }

    public List<ProductCatalogueProduct> getProductInformation(String cartId){

        String productsFromProductCatalogue = this.getProductInformationString(cartId);

        List<ProductCatalogueProduct> productCatalogueProductList =
            this.jsonParser.parseJSONArrayToProductCatalogueProductList(productsFromProductCatalogue);

        return productCatalogueProductList;
    }

    public String getProductInformationString(String cartId){
        JSONArray productsInCart = this.getCartProductID(cartId);

        JSONArray productsInCartIds = this.jsonParser.createProductIdList(productsInCart);

        String productsFromProductCatalogue =
            this.productCatalogueCommunicator.getProductsFromCatalogue(productsInCartIds);

        return productsFromProductCatalogue;
    }

    /**
     * Finalize the checkout
     * @param placeOrderInformation which includes Currency, Checkout Person and Cart ID
     * @return
     */

    public String finalizeCheckout(PlaceOrderInformation placeOrderInformation) {

        String productInformation = this.getProductInformationString(placeOrderInformation.getCartId());

        List<Product> productsInCartList =
            this.jsonParser.parseStringToProductList(productInformation);

        PersonProductObject personProductObject = new PersonProductObject(productsInCartList
            , placeOrderInformation.getCheckoutPerson());

        Integer shippingCost =
            Integer.parseInt(this.shippingCommunicator.getShippingPrice(this.jsonParser.transferPersonProductToJsonObject(personProductObject)));

        productsInCartList.add(new Product ("shippingCost", shippingCost));

        if(placeOrderInformation.getCurrency().equals("USD")){

            String newProductPrices = this.currencyCommunicator.calculatePrices("USD",
                jsonParser.createProductArray(productsInCartList));

            productsInCartList =
                this.jsonParser.parseJSONArrayToProductList(this.jsonParser.parseArray(newProductPrices));

        }

        Integer costToPay = this.sumUpCosts(productsInCartList);

        String payment =
            this.paymentCommunicator.chargeClient(this.jsonParser.parsePersonPaymentObject(new PersonPaymentObject(placeOrderInformation.getCheckoutPerson(),
        costToPay)));

        String shipping =
            this.shippingCommunicator.shipProducts(this.jsonParser.transferPersonProductToJsonObject(personProductObject));

        String email =
            this.eMailCommunicator.sendEMail(this.jsonParser.createPerson(placeOrderInformation.getCheckoutPerson()));

        String responseText =
            "Order placed! " + payment + " " + placeOrderInformation.getCurrency() +
            ". " + shipping + ". " + email + ".";

        return responseText;
    }

    public void emptyCart(String cartId){

        System.out.println("Cart to empty ID: " + cartId);

        JSONArray jsonArray = this.getCartProductID(cartId);

        System.out.println("Products in Cart String: " + jsonArray.toString());

        this.cartCommunicator.removeProducts(jsonArray, cartId);
    }

    /**
     * sum up all costs for a delivery
     * @param allCosts
     * @return
     */
    private Integer sumUpCosts(List<Product> allCosts){
        Integer summedupCosts = 0;

        for(Product product: allCosts){
            summedupCosts += product.getPrice();
        }

        return summedupCosts;
    }





    /*
    public JSONArray testCurrencyCommunicator(List<Product> productList){
        return this.jsonParser.parseArray(this.currencyCommunicator.calculatePrices("USD",
            jsonParser.createProductArray(productList)));
    }

    public String testShippingCosts(PersonProductObject personProductObject){

        return this.shippingCommunicator.getShippingPrice(this.jsonParser.transferPersonProductToJsonObject(personProductObject));
    }

    public String testProductShipping(PersonProductObject personProductObject){

        return this.shippingCommunicator.shipProducts(this.jsonParser.transferPersonProductToJsonObject(personProductObject));
    }

    public String testEmail(Person person){

        return this.eMailCommunicator.sendEMail(this.jsonParser.createPerson(person));
    }

    public String testPayment(PersonPaymentObject personPaymentObject){

        return this.paymentCommunicator.chargeClient(this.jsonParser.parsePersonPaymentObject(personPaymentObject));
    }

    public String testProductCatalogue(List<String> productIds){
        return this.productCatalogueCommunicator.getProductsFromCatalogue(jsonParser.parseArray(productIds.toString()));
    }
    */
}
