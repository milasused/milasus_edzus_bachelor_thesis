package milasus.shop.checkout.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Checkout.
 */
@Document(collection = "checkout")
public class Checkout implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("e_mail")
    private String eMail;

    @Field("first_name")
    private String firstName;

    @NotNull
    @Field("last_name")
    private String lastName;

    @Field("address")
    private String address;

    @Field("credit_card_number")
    private String creditCardNumber;

    @Field("credit_card_security_number")
    private Integer creditCardSecurityNumber;

    @Field("credit_card_expiration_date")
    private Integer creditCardExpirationDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String geteMail() {
        return eMail;
    }

    public Checkout eMail(String eMail) {
        this.eMail = eMail;
        return this;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getFirstName() {
        return firstName;
    }

    public Checkout firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Checkout lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public Checkout address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public Checkout creditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
        return this;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public Integer getCreditCardSecurityNumber() {
        return creditCardSecurityNumber;
    }

    public Checkout creditCardSecurityNumber(Integer creditCardSecurityNumber) {
        this.creditCardSecurityNumber = creditCardSecurityNumber;
        return this;
    }

    public void setCreditCardSecurityNumber(Integer creditCardSecurityNumber) {
        this.creditCardSecurityNumber = creditCardSecurityNumber;
    }

    public Integer getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    public Checkout creditCardExpirationDate(Integer creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
        return this;
    }

    public void setCreditCardExpirationDate(Integer creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Checkout)) {
            return false;
        }
        return id != null && id.equals(((Checkout) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Checkout{" +
            "id=" + getId() +
            ", eMail='" + geteMail() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", address='" + getAddress() + "'" +
            ", creditCartNumber='" + getCreditCardNumber() + "'" +
            ", creditCartSecurityNumber=" + getCreditCardSecurityNumber() +
            ", creditCardExpirationMonth=" + getCreditCardExpirationDate() +
            "}";
    }
}
