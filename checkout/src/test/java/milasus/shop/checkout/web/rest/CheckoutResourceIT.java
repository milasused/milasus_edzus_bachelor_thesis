package milasus.shop.checkout.web.rest;

import milasus.shop.checkout.CheckoutApp;
import milasus.shop.checkout.domain.Checkout;
import milasus.shop.checkout.repository.CheckoutRepository;
import milasus.shop.checkout.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static milasus.shop.checkout.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CheckoutResource} REST controller.
 */
@SpringBootTest(classes = CheckoutApp.class)
public class CheckoutResourceIT {

    private static final String DEFAULT_E_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_E_MAIL = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CREDIT_CART_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CREDIT_CART_NUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_CREDIT_CART_SECURITY_NUMBER = 1;
    private static final Integer UPDATED_CREDIT_CART_SECURITY_NUMBER = 2;
    private static final Integer SMALLER_CREDIT_CART_SECURITY_NUMBER = 1 - 1;

    private static final Integer DEFAULT_CREDIT_CARD_EXPIRATION_DATE = 1;
    private static final Integer UPDATED_CREDIT_CARD_EXPIRATION_DATE = 2;
    private static final Integer SMALLER_CREDIT_CARD_EXPIRATION_DATE = 1 - 1;

    @Autowired
    private CheckoutRepository checkoutRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restCheckoutMockMvc;

    private Checkout checkout;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CheckoutResource checkoutResource = new CheckoutResource(checkoutRepository);
        this.restCheckoutMockMvc = MockMvcBuilders.standaloneSetup(checkoutResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Checkout createEntity() {
        Checkout checkout = new Checkout()
            .eMail(DEFAULT_E_MAIL)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .address(DEFAULT_ADDRESS)
            .creditCardNumber(DEFAULT_CREDIT_CART_NUMBER)
            .creditCardSecurityNumber(DEFAULT_CREDIT_CART_SECURITY_NUMBER)
            .creditCardExpirationDate(DEFAULT_CREDIT_CARD_EXPIRATION_DATE);
        return checkout;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Checkout createUpdatedEntity() {
        Checkout checkout = new Checkout()
            .eMail(UPDATED_E_MAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .address(UPDATED_ADDRESS)
            .creditCardNumber(UPDATED_CREDIT_CART_NUMBER)
            .creditCardSecurityNumber(UPDATED_CREDIT_CART_SECURITY_NUMBER)
            .creditCardExpirationDate(UPDATED_CREDIT_CARD_EXPIRATION_DATE);
        return checkout;
    }

    @BeforeEach
    public void initTest() {
        checkoutRepository.deleteAll();
        checkout = createEntity();
    }

    @Test
    public void createCheckout() throws Exception {
        int databaseSizeBeforeCreate = checkoutRepository.findAll().size();

        // Create the Checkout
        restCheckoutMockMvc.perform(post("/api/checkouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkout)))
            .andExpect(status().isCreated());

        // Validate the Checkout in the database
        List<Checkout> checkoutList = checkoutRepository.findAll();
        assertThat(checkoutList).hasSize(databaseSizeBeforeCreate + 1);
        Checkout testCheckout = checkoutList.get(checkoutList.size() - 1);
        assertThat(testCheckout.geteMail()).isEqualTo(DEFAULT_E_MAIL);
        assertThat(testCheckout.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testCheckout.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testCheckout.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testCheckout.getCreditCardNumber()).isEqualTo(DEFAULT_CREDIT_CART_NUMBER);
        assertThat(testCheckout.getCreditCardSecurityNumber()).isEqualTo(DEFAULT_CREDIT_CART_SECURITY_NUMBER);
        assertThat(testCheckout.getCreditCardExpirationDate()).isEqualTo(DEFAULT_CREDIT_CARD_EXPIRATION_DATE);
    }

    @Test
    public void createCheckoutWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = checkoutRepository.findAll().size();

        // Create the Checkout with an existing ID
        checkout.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCheckoutMockMvc.perform(post("/api/checkouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkout)))
            .andExpect(status().isBadRequest());

        // Validate the Checkout in the database
        List<Checkout> checkoutList = checkoutRepository.findAll();
        assertThat(checkoutList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = checkoutRepository.findAll().size();
        // set the field null
        checkout.setLastName(null);

        // Create the Checkout, which fails.

        restCheckoutMockMvc.perform(post("/api/checkouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkout)))
            .andExpect(status().isBadRequest());

        List<Checkout> checkoutList = checkoutRepository.findAll();
        assertThat(checkoutList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllCheckouts() throws Exception {
        // Initialize the database
        checkoutRepository.save(checkout);

        // Get all the checkoutList
        restCheckoutMockMvc.perform(get("/api/checkouts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(checkout.getId())))
            .andExpect(jsonPath("$.[*].eMail").value(hasItem(DEFAULT_E_MAIL.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].street").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].creditCartNumber").value(hasItem(DEFAULT_CREDIT_CART_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].creditCartSecurityNumber").value(hasItem(DEFAULT_CREDIT_CART_SECURITY_NUMBER)))
            .andExpect(jsonPath("$.[*].creditCardExpirationMonth").value(hasItem(DEFAULT_CREDIT_CARD_EXPIRATION_DATE)));
    }

    @Test
    public void getCheckout() throws Exception {
        // Initialize the database
        checkoutRepository.save(checkout);

        // Get the checkout
        restCheckoutMockMvc.perform(get("/api/checkouts/{id}", checkout.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(checkout.getId()))
            .andExpect(jsonPath("$.eMail").value(DEFAULT_E_MAIL.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.street").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.creditCartNumber").value(DEFAULT_CREDIT_CART_NUMBER.toString()))
            .andExpect(jsonPath("$.creditCartSecurityNumber").value(DEFAULT_CREDIT_CART_SECURITY_NUMBER))
            .andExpect(jsonPath("$.creditCardExpirationMonth").value(DEFAULT_CREDIT_CARD_EXPIRATION_DATE));
    }

    @Test
    public void getNonExistingCheckout() throws Exception {
        // Get the checkout
        restCheckoutMockMvc.perform(get("/api/checkouts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCheckout() throws Exception {
        // Initialize the database
        checkoutRepository.save(checkout);

        int databaseSizeBeforeUpdate = checkoutRepository.findAll().size();

        // Update the checkout
        Checkout updatedCheckout = checkoutRepository.findById(checkout.getId()).get();
        updatedCheckout
            .eMail(UPDATED_E_MAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .address(UPDATED_ADDRESS)
            .creditCardNumber(UPDATED_CREDIT_CART_NUMBER)
            .creditCardSecurityNumber(UPDATED_CREDIT_CART_SECURITY_NUMBER)
            .creditCardExpirationDate(UPDATED_CREDIT_CARD_EXPIRATION_DATE);

        restCheckoutMockMvc.perform(put("/api/checkouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCheckout)))
            .andExpect(status().isOk());

        // Validate the Checkout in the database
        List<Checkout> checkoutList = checkoutRepository.findAll();
        assertThat(checkoutList).hasSize(databaseSizeBeforeUpdate);
        Checkout testCheckout = checkoutList.get(checkoutList.size() - 1);
        assertThat(testCheckout.geteMail()).isEqualTo(UPDATED_E_MAIL);
        assertThat(testCheckout.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testCheckout.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testCheckout.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testCheckout.getCreditCardNumber()).isEqualTo(UPDATED_CREDIT_CART_NUMBER);
        assertThat(testCheckout.getCreditCardSecurityNumber()).isEqualTo(UPDATED_CREDIT_CART_SECURITY_NUMBER);
        assertThat(testCheckout.getCreditCardExpirationDate()).isEqualTo(UPDATED_CREDIT_CARD_EXPIRATION_DATE);
    }

    @Test
    public void updateNonExistingCheckout() throws Exception {
        int databaseSizeBeforeUpdate = checkoutRepository.findAll().size();

        // Create the Checkout

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCheckoutMockMvc.perform(put("/api/checkouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkout)))
            .andExpect(status().isBadRequest());

        // Validate the Checkout in the database
        List<Checkout> checkoutList = checkoutRepository.findAll();
        assertThat(checkoutList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCheckout() throws Exception {
        // Initialize the database
        checkoutRepository.save(checkout);

        int databaseSizeBeforeDelete = checkoutRepository.findAll().size();

        // Delete the checkout
        restCheckoutMockMvc.perform(delete("/api/checkouts/{id}", checkout.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Checkout> checkoutList = checkoutRepository.findAll();
        assertThat(checkoutList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Checkout.class);
        Checkout checkout1 = new Checkout();
        checkout1.setId("id1");
        Checkout checkout2 = new Checkout();
        checkout2.setId(checkout1.getId());
        assertThat(checkout1).isEqualTo(checkout2);
        checkout2.setId("id2");
        assertThat(checkout1).isNotEqualTo(checkout2);
        checkout1.setId(null);
        assertThat(checkout1).isNotEqualTo(checkout2);
    }
}
