package milasus.shop.checkout.web.rest;

import milasus.shop.checkout.CheckoutApp;
import milasus.shop.checkout.domain.CheckoutProduct;
import milasus.shop.checkout.repository.CheckoutProductRepository;
import milasus.shop.checkout.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static milasus.shop.checkout.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CheckoutProductResource} REST controller.
 */
@SpringBootTest(classes = CheckoutApp.class)
public class CheckoutProductResourceIT {

    @Autowired
    private CheckoutProductRepository checkoutProductRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restCheckoutProductMockMvc;

    private CheckoutProduct checkoutProduct;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CheckoutProductResource checkoutProductResource = new CheckoutProductResource(checkoutProductRepository);
        this.restCheckoutProductMockMvc = MockMvcBuilders.standaloneSetup(checkoutProductResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CheckoutProduct createEntity() {
        CheckoutProduct checkoutProduct = new CheckoutProduct();
        return checkoutProduct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CheckoutProduct createUpdatedEntity() {
        CheckoutProduct checkoutProduct = new CheckoutProduct();
        return checkoutProduct;
    }

    @BeforeEach
    public void initTest() {
        checkoutProductRepository.deleteAll();
        checkoutProduct = createEntity();
    }

    @Test
    public void createCheckoutProduct() throws Exception {
        int databaseSizeBeforeCreate = checkoutProductRepository.findAll().size();

        // Create the CheckoutProduct
        restCheckoutProductMockMvc.perform(post("/api/checkout-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkoutProduct)))
            .andExpect(status().isCreated());

        // Validate the CheckoutProduct in the database
        List<CheckoutProduct> checkoutProductList = checkoutProductRepository.findAll();
        assertThat(checkoutProductList).hasSize(databaseSizeBeforeCreate + 1);
        CheckoutProduct testCheckoutProduct = checkoutProductList.get(checkoutProductList.size() - 1);
    }

    @Test
    public void createCheckoutProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = checkoutProductRepository.findAll().size();

        // Create the CheckoutProduct with an existing ID
        checkoutProduct.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCheckoutProductMockMvc.perform(post("/api/checkout-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkoutProduct)))
            .andExpect(status().isBadRequest());

        // Validate the CheckoutProduct in the database
        List<CheckoutProduct> checkoutProductList = checkoutProductRepository.findAll();
        assertThat(checkoutProductList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCheckoutProducts() throws Exception {
        // Initialize the database
        checkoutProductRepository.save(checkoutProduct);

        // Get all the checkoutProductList
        restCheckoutProductMockMvc.perform(get("/api/checkout-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(checkoutProduct.getId())));
    }
    
    @Test
    public void getCheckoutProduct() throws Exception {
        // Initialize the database
        checkoutProductRepository.save(checkoutProduct);

        // Get the checkoutProduct
        restCheckoutProductMockMvc.perform(get("/api/checkout-products/{id}", checkoutProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(checkoutProduct.getId()));
    }

    @Test
    public void getNonExistingCheckoutProduct() throws Exception {
        // Get the checkoutProduct
        restCheckoutProductMockMvc.perform(get("/api/checkout-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCheckoutProduct() throws Exception {
        // Initialize the database
        checkoutProductRepository.save(checkoutProduct);

        int databaseSizeBeforeUpdate = checkoutProductRepository.findAll().size();

        // Update the checkoutProduct
        CheckoutProduct updatedCheckoutProduct = checkoutProductRepository.findById(checkoutProduct.getId()).get();

        restCheckoutProductMockMvc.perform(put("/api/checkout-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCheckoutProduct)))
            .andExpect(status().isOk());

        // Validate the CheckoutProduct in the database
        List<CheckoutProduct> checkoutProductList = checkoutProductRepository.findAll();
        assertThat(checkoutProductList).hasSize(databaseSizeBeforeUpdate);
        CheckoutProduct testCheckoutProduct = checkoutProductList.get(checkoutProductList.size() - 1);
    }

    @Test
    public void updateNonExistingCheckoutProduct() throws Exception {
        int databaseSizeBeforeUpdate = checkoutProductRepository.findAll().size();

        // Create the CheckoutProduct

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCheckoutProductMockMvc.perform(put("/api/checkout-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkoutProduct)))
            .andExpect(status().isBadRequest());

        // Validate the CheckoutProduct in the database
        List<CheckoutProduct> checkoutProductList = checkoutProductRepository.findAll();
        assertThat(checkoutProductList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCheckoutProduct() throws Exception {
        // Initialize the database
        checkoutProductRepository.save(checkoutProduct);

        int databaseSizeBeforeDelete = checkoutProductRepository.findAll().size();

        // Delete the checkoutProduct
        restCheckoutProductMockMvc.perform(delete("/api/checkout-products/{id}", checkoutProduct.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CheckoutProduct> checkoutProductList = checkoutProductRepository.findAll();
        assertThat(checkoutProductList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CheckoutProduct.class);
        CheckoutProduct checkoutProduct1 = new CheckoutProduct();
        checkoutProduct1.setId("id1");
        CheckoutProduct checkoutProduct2 = new CheckoutProduct();
        checkoutProduct2.setId(checkoutProduct1.getId());
        assertThat(checkoutProduct1).isEqualTo(checkoutProduct2);
        checkoutProduct2.setId("id2");
        assertThat(checkoutProduct1).isNotEqualTo(checkoutProduct2);
        checkoutProduct1.setId(null);
        assertThat(checkoutProduct1).isNotEqualTo(checkoutProduct2);
    }
}
