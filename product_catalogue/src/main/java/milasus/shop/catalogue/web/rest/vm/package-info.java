/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.catalogue.web.rest.vm;
