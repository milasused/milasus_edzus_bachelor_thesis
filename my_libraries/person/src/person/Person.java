package person;

/**
 * Person class used to represent customers
 */
public class Person {

    String firstName;
    String lastName;
    String address;
    String eMailAddress;
    Integer creditCardNumber;
    Integer creditCardSecurityNumber;
    Integer creditCardExpirationDate;

    /**
     * Create a person object with first and last name
     * @param firstName
     * @param lastName
     */
    public Person(String firstName, String lastName){
        this.setPersonName(firstName, lastName);
    }

    /**
     * Create a Person object with first name, last name and address
     * @param firstName
     * @param lastName
     * @param address
     */
    public Person(String firstName, String lastName, String address){
        this.setPersonName(firstName, lastName);
        this.setAddress(address);
    }

    public Person(){}

    public Person(String firstName, String lastName, String address, String eMailAddress
            , Integer creditCardNumber, Integer creditCardSecurityNumber
            , Integer creditCardExpirationDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.eMailAddress = eMailAddress;
        this.creditCardNumber = creditCardNumber;
        this.creditCardSecurityNumber = creditCardSecurityNumber;
        this.creditCardExpirationDate = creditCardExpirationDate;
    }

    private void setPersonName(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String geteMailAddress() {
        return eMailAddress;
    }

    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(Integer creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public Integer getCreditCardSecurityNumber() {
        return creditCardSecurityNumber;
    }

    public void setCreditCardSecurityNumber(Integer creditCardSecurityNumber) {
        this.creditCardSecurityNumber = creditCardSecurityNumber;
    }

    public Integer getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    public void setCreditCardExpirationDate(Integer creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
    }
}
