package personPayment;

import person.Person;

public class PersonPaymentObject {
  Person person;
  Integer amountToPay;

  public PersonPaymentObject() {
  }

  public PersonPaymentObject(Person person, Integer amountToPay) {
    this.person = person;
    this.amountToPay = amountToPay;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public Integer getAmountToPay() {
    return amountToPay;
  }

  public void setAmountToPay(Integer amountToPay) {
    this.amountToPay = amountToPay;
  }
}
