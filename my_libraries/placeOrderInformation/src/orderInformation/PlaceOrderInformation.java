package orderInformation;

import person.Person;

public class PlaceOrderInformation {

  private String cartId;
  private Person checkoutPerson;
  private String currency;

  public PlaceOrderInformation() {
  }

  public PlaceOrderInformation(String cartId, Person checkoutPerson, String currency) {
    this.cartId = cartId;
    this.checkoutPerson = checkoutPerson;
    this.currency = currency;
  }

  public String getCartId() {
    return cartId;
  }

  public void setCartId(String cartId) {
    this.cartId = cartId;
  }

  public Person getCheckoutPerson() {
    return checkoutPerson;
  }

  public void setCheckoutPerson(Person checkoutPerson) {
    this.checkoutPerson = checkoutPerson;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }
}
