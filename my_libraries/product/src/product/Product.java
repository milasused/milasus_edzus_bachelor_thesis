package product;

import exceptions.ProductIdException;
import exceptions.ProductPriceException;

public class Product {

    private String id;
    private Integer price;

    public Product(String id, Integer price) {
        this.id = id;
        this.price = price;
    }

    public Product(String id) {
        this.id = id;
    }

    public Product(){}

    public String getId() {
        return id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    // TODO: insert getDescription, getName etc. with the help of Spring Requests to the product_catalogue service

    public void checkPrice() throws ProductPriceException {
        if(this.price < 0){
            throw new ProductPriceException("Price of product must not be negative");
        }
    }

    public void checkId() throws ProductIdException {
        if(this.id == null){
            throw new ProductIdException("Product ID may not be null");
        }
    }
}
