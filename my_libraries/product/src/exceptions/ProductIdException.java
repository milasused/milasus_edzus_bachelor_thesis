package exceptions;

public class ProductIdException extends Exception{
  public ProductIdException() {
  }

  public ProductIdException(String message) {
    super(message);
  }

  public ProductIdException(String message, Throwable cause) {
    super(message, cause);
  }

  public ProductIdException(Throwable cause) {
    super(cause);
  }

  public ProductIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
