package exceptions;

public class ProductPriceException extends Exception {
  public ProductPriceException() {
  }

  public ProductPriceException(String message) {
    super(message);
  }

  public ProductPriceException(String message, Throwable cause) {
    super(message, cause);
  }

  public ProductPriceException(Throwable cause) {
    super(cause);
  }

  public ProductPriceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
