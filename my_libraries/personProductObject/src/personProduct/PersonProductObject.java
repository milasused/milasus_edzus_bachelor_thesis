package personProduct;

import person.Person;
import product.Product;

import java.util.List;

/**
 * Person Product wrapper class for easier communication
 */
public class PersonProductObject {

  private List<Product> productList;
  private Person person;

  public PersonProductObject(List<Product> productList, Person person) {
    this.productList = productList;
    this.person = person;
  }

  public PersonProductObject(){}

  public List<Product> getProductList() {
    return productList;
  }

  public void setProductList(List<Product> productList) {
    this.productList = productList;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }
}
