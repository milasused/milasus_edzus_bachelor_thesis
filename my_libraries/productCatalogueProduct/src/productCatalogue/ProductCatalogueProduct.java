package productCatalogue;

public class ProductCatalogueProduct {

  private String id;
  private String name;
  private Integer price;
  private String description;
  private String fotoPath;

  public ProductCatalogueProduct() {
  }

  public ProductCatalogueProduct(String id, String name, Integer price, String description, String fotoPath) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.description = description;
    this.fotoPath = fotoPath;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFotoPath() {
    return fotoPath;
  }

  public void setFotoPath(String fotoPath) {
    this.fotoPath = fotoPath;
  }
}
