/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.cart.web.rest.vm;
