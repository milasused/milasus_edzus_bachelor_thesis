package milasus.shop.cart.domain;
import exceptions.ProductIdException;
import exceptions.ProductPriceException;
import milasus.shop.cart.web.rest.CartResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import product.Product;

import java.io.Serializable;
import java.util.List;

/**
 * A Cart.
 */
@Document(collection = "cart")
public class Cart implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private List<Product> productList;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cart)) {
            return false;
        }
        return id != null && id.equals(((Cart) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cart{" +
            "id=" + getId() +
            "}";
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void addProduct(Product product) throws ProductIdException, ProductPriceException {
        product.checkId();
        product.checkPrice();
        this.productList.add(product);
    }

    public Boolean removeProduct(Product product){

        Boolean removeResult = false;

        for(int i = 0; i < this.productList.size(); ++i){
            if(this.productList.get(i).getId().equals(product.getId())){
                this.productList.remove(i);
                removeResult = true;
                break;
            }
        }

        return removeResult;
    }
}
