package milasus.shop.cart.exception;

/**
 * Exception is used by CartLogic class to signal, that the product has not been found in the cart
 */
public class ProductNotFoundException extends Exception {

    public ProductNotFoundException(String errorMessage){
        super(errorMessage);
    }

}
