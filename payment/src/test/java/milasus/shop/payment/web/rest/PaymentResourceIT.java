package milasus.shop.payment.web.rest;

import milasus.shop.payment.PaymentApp;
import milasus.shop.payment.domain.Payment;
import milasus.shop.payment.repository.PaymentRepository;
import milasus.shop.payment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static milasus.shop.payment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PaymentResource} REST controller.
 */
@SpringBootTest(classes = PaymentApp.class)
public class PaymentResourceIT {

    private static final String DEFAULT_CUSTOMER_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_LAST_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_CREDIT_CARD_NUMBER = 1;
    private static final Integer UPDATED_CREDIT_CARD_NUMBER = 2;
    private static final Integer SMALLER_CREDIT_CARD_NUMBER = 1 - 1;

    private static final Integer DEFAULT_CREDIT_CARD_SECURITY_NUMBER = 1;
    private static final Integer UPDATED_CREDIT_CARD_SECURITY_NUMBER = 2;
    private static final Integer SMALLER_CREDIT_CARD_SECURITY_NUMBER = 1 - 1;

    private static final Integer DEFAULT_CREDIT_CARD_EXPIRATION_DATE = 1;
    private static final Integer UPDATED_CREDIT_CARD_EXPIRATION_DATE = 2;
    private static final Integer SMALLER_CREDIT_CARD_EXPIRATION_DATE = 1 - 1;

    private static final Integer DEFAULT_AMOUNT_TO_PAY = 1;
    private static final Integer UPDATED_AMOUNT_TO_PAY = 2;
    private static final Integer SMALLER_AMOUNT_TO_PAY = 1 - 1;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restPaymentMockMvc;

    private Payment payment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaymentResource paymentResource = new PaymentResource(paymentRepository);
        this.restPaymentMockMvc = MockMvcBuilders.standaloneSetup(paymentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createEntity() {
        Payment payment = new Payment()
            .customerLastName(DEFAULT_CUSTOMER_LAST_NAME)
            .creditCardNumber(DEFAULT_CREDIT_CARD_NUMBER)
            .creditCardSecurityNumber(DEFAULT_CREDIT_CARD_SECURITY_NUMBER)
            .creditCardExpirationDate(DEFAULT_CREDIT_CARD_EXPIRATION_DATE)
            .amountToPay(DEFAULT_AMOUNT_TO_PAY);
        return payment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createUpdatedEntity() {
        Payment payment = new Payment()
            .customerLastName(UPDATED_CUSTOMER_LAST_NAME)
            .creditCardNumber(UPDATED_CREDIT_CARD_NUMBER)
            .creditCardSecurityNumber(UPDATED_CREDIT_CARD_SECURITY_NUMBER)
            .creditCardExpirationDate(UPDATED_CREDIT_CARD_EXPIRATION_DATE)
            .amountToPay(UPDATED_AMOUNT_TO_PAY);
        return payment;
    }

    @BeforeEach
    public void initTest() {
        paymentRepository.deleteAll();
        payment = createEntity();
    }

    @Test
    public void createPayment() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // Create the Payment
        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isCreated());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate + 1);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getCustomerLastName()).isEqualTo(DEFAULT_CUSTOMER_LAST_NAME);
        assertThat(testPayment.getCreditCardNumber()).isEqualTo(DEFAULT_CREDIT_CARD_NUMBER);
        assertThat(testPayment.getCreditCardSecurityNumber()).isEqualTo(DEFAULT_CREDIT_CARD_SECURITY_NUMBER);
        assertThat(testPayment.getCreditCardExpirationDate()).isEqualTo(DEFAULT_CREDIT_CARD_EXPIRATION_DATE);
        assertThat(testPayment.getAmountToPay()).isEqualTo(DEFAULT_AMOUNT_TO_PAY);
    }

    @Test
    public void createPaymentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // Create the Payment with an existing ID
        payment.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllPayments() throws Exception {
        // Initialize the database
        paymentRepository.save(payment);

        // Get all the paymentList
        restPaymentMockMvc.perform(get("/api/payments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId())))
            .andExpect(jsonPath("$.[*].customerLastName").value(hasItem(DEFAULT_CUSTOMER_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].creditCardNumber").value(hasItem(DEFAULT_CREDIT_CARD_NUMBER)))
            .andExpect(jsonPath("$.[*].creditCardSecurityNumber").value(hasItem(DEFAULT_CREDIT_CARD_SECURITY_NUMBER)))
            .andExpect(jsonPath("$.[*].creditCardExpirationDate").value(hasItem(DEFAULT_CREDIT_CARD_EXPIRATION_DATE)))
            .andExpect(jsonPath("$.[*].amountToPay").value(hasItem(DEFAULT_AMOUNT_TO_PAY)));
    }
    
    @Test
    public void getPayment() throws Exception {
        // Initialize the database
        paymentRepository.save(payment);

        // Get the payment
        restPaymentMockMvc.perform(get("/api/payments/{id}", payment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(payment.getId()))
            .andExpect(jsonPath("$.customerLastName").value(DEFAULT_CUSTOMER_LAST_NAME.toString()))
            .andExpect(jsonPath("$.creditCardNumber").value(DEFAULT_CREDIT_CARD_NUMBER))
            .andExpect(jsonPath("$.creditCardSecurityNumber").value(DEFAULT_CREDIT_CARD_SECURITY_NUMBER))
            .andExpect(jsonPath("$.creditCardExpirationDate").value(DEFAULT_CREDIT_CARD_EXPIRATION_DATE))
            .andExpect(jsonPath("$.amountToPay").value(DEFAULT_AMOUNT_TO_PAY));
    }

    @Test
    public void getNonExistingPayment() throws Exception {
        // Get the payment
        restPaymentMockMvc.perform(get("/api/payments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePayment() throws Exception {
        // Initialize the database
        paymentRepository.save(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment
        Payment updatedPayment = paymentRepository.findById(payment.getId()).get();
        updatedPayment
            .customerLastName(UPDATED_CUSTOMER_LAST_NAME)
            .creditCardNumber(UPDATED_CREDIT_CARD_NUMBER)
            .creditCardSecurityNumber(UPDATED_CREDIT_CARD_SECURITY_NUMBER)
            .creditCardExpirationDate(UPDATED_CREDIT_CARD_EXPIRATION_DATE)
            .amountToPay(UPDATED_AMOUNT_TO_PAY);

        restPaymentMockMvc.perform(put("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPayment)))
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getCustomerLastName()).isEqualTo(UPDATED_CUSTOMER_LAST_NAME);
        assertThat(testPayment.getCreditCardNumber()).isEqualTo(UPDATED_CREDIT_CARD_NUMBER);
        assertThat(testPayment.getCreditCardSecurityNumber()).isEqualTo(UPDATED_CREDIT_CARD_SECURITY_NUMBER);
        assertThat(testPayment.getCreditCardExpirationDate()).isEqualTo(UPDATED_CREDIT_CARD_EXPIRATION_DATE);
        assertThat(testPayment.getAmountToPay()).isEqualTo(UPDATED_AMOUNT_TO_PAY);
    }

    @Test
    public void updateNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Create the Payment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc.perform(put("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deletePayment() throws Exception {
        // Initialize the database
        paymentRepository.save(payment);

        int databaseSizeBeforeDelete = paymentRepository.findAll().size();

        // Delete the payment
        restPaymentMockMvc.perform(delete("/api/payments/{id}", payment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Payment.class);
        Payment payment1 = new Payment();
        payment1.setId("id1");
        Payment payment2 = new Payment();
        payment2.setId(payment1.getId());
        assertThat(payment1).isEqualTo(payment2);
        payment2.setId("id2");
        assertThat(payment1).isNotEqualTo(payment2);
        payment1.setId(null);
        assertThat(payment1).isNotEqualTo(payment2);
    }
}
