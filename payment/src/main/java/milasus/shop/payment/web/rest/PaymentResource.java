package milasus.shop.payment.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import milasus.shop.payment.domain.Payment;
import milasus.shop.payment.exception.IncorrectPersonPaymentObjectRuntimeException;
import milasus.shop.payment.repository.PaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import person.Person;
import personPayment.PersonPaymentObject;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link milasus.shop.payment.domain.Payment}.
 */
@RestController
@RequestMapping("/api/payment")
public class PaymentResource {

    private final Logger log = LoggerFactory.getLogger(PaymentResource.class);

    private static final String ENTITY_NAME = "paymentPayment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentRepository paymentRepository;

    public PaymentResource(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    /**
     * {@code GET  /payments} : get all the payments.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of payments in body.
     */
    @GetMapping("/payments")
    public List<Payment> getAllPayments() {
        log.debug("REST request to get all Payments");
        return paymentRepository.findAll();
    }

    /**
     * {@code GET  /payments/:id} : get the "id" payment.
     *
     * @param id the id of the payment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the payment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payments/{id}")
    public ResponseEntity<Payment> getPayment(@PathVariable String id) {
        log.debug("REST request to get Payment : {}", id);
        Optional<Payment> payment = paymentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(payment);
    }

    /**
     * {@code DELETE  /payments/:id} : delete the "id" payment.
     *
     * @param id the id of the payment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Void> deletePayment(@PathVariable String id) {
        log.debug("REST request to delete Payment : {}", id);
        paymentRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    /**
     * POST newPayment
     *
     */
    @PostMapping("/new-payment")
    public String newPayment(@RequestBody PersonPaymentObject personPaymentObject) {

        this.creditCardValidityCheck(personPaymentObject.getPerson());

        this.checkPayment(personPaymentObject.getAmountToPay());

        log.debug("REST request to save Payment : {}", personPaymentObject);
        Payment paymentToSave = new Payment();
        paymentToSave.setAmountToPay(personPaymentObject.getAmountToPay());
        paymentToSave.setCreditCardExpirationDate(personPaymentObject.getPerson().getCreditCardExpirationDate());
        paymentToSave.setCreditCardNumber(personPaymentObject.getPerson().getCreditCardNumber());
        paymentToSave.setCreditCardSecurityNumber(personPaymentObject.getPerson().getCreditCardSecurityNumber());
        paymentToSave.setCustomerLastName(personPaymentObject.getPerson().getLastName());

        Payment result = paymentRepository.save(paymentToSave);

        return "Payment from " + personPaymentObject.getPerson().getFirstName() + " " + personPaymentObject.getPerson().getLastName() + " " +
            "received! Amount payed: " + personPaymentObject.getAmountToPay();
    }

    private void creditCardValidityCheck(Person person){
        if(person.getCreditCardExpirationDate() == null || person.getCreditCardNumber() == null || person.getCreditCardSecurityNumber() == null){
            throw new IncorrectPersonPaymentObjectRuntimeException("Credit Card Data is not correct");
        }
    }

    private void checkPayment(Integer amountToPay){
        if(amountToPay == null || amountToPay < 0){
            throw new IncorrectPersonPaymentObjectRuntimeException("Invalid amount to pay");
        }
    }

}
