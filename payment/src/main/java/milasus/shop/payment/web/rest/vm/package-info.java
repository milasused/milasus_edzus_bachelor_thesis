/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.payment.web.rest.vm;
