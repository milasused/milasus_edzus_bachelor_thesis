package milasus.shop.payment.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * A Payment.
 */
@Document(collection = "payment")
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("customer_last_name")
    private String customerLastName;

    @Field("credit_card_number")
    private Integer creditCardNumber;

    @Field("credit_card_security_number")
    private Integer creditCardSecurityNumber;

    @Field("credit_card_expiration_date")
    private Integer creditCardExpirationDate;

    @Field("amount_to_pay")
    private Integer amountToPay;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public Payment customerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
        return this;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public Integer getCreditCardNumber() {
        return creditCardNumber;
    }

    public Payment creditCardNumber(Integer creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
        return this;
    }

    public void setCreditCardNumber(Integer creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public Integer getCreditCardSecurityNumber() {
        return creditCardSecurityNumber;
    }

    public Payment creditCardSecurityNumber(Integer creditCardSecurityNumber) {
        this.creditCardSecurityNumber = creditCardSecurityNumber;
        return this;
    }

    public void setCreditCardSecurityNumber(Integer creditCardSecurityNumber) {
        this.creditCardSecurityNumber = creditCardSecurityNumber;
    }

    public Integer getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    public Payment creditCardExpirationDate(Integer creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
        return this;
    }

    public void setCreditCardExpirationDate(Integer creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
    }

    public Integer getAmountToPay() {
        return amountToPay;
    }

    public Payment amountToPay(Integer amountToPay) {
        this.amountToPay = amountToPay;
        return this;
    }

    public void setAmountToPay(Integer amountToPay) {
        this.amountToPay = amountToPay;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Payment)) {
            return false;
        }
        return id != null && id.equals(((Payment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", customerLastName='" + getCustomerLastName() + "'" +
            ", creditCardNumber=" + getCreditCardNumber() +
            ", creditCardSecurityNumber=" + getCreditCardSecurityNumber() +
            ", creditCardExpirationDate=" + getCreditCardExpirationDate() +
            ", amountToPay=" + getAmountToPay() +
            "}";
    }
}
