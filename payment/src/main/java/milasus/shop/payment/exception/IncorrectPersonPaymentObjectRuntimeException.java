package milasus.shop.payment.exception;

/**
 * Credit Card Data is not correct Exception
 */
public class IncorrectPersonPaymentObjectRuntimeException extends RuntimeException{

    public IncorrectPersonPaymentObjectRuntimeException(String errorMessage){
        super(errorMessage);
    }

}
