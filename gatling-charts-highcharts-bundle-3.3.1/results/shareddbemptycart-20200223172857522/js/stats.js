var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "16212",
        "ok": "2172",
        "ko": "14040"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "1174"
    },
    "maxResponseTime": {
        "total": "60025",
        "ok": "841",
        "ko": "60025"
    },
    "meanResponseTime": {
        "total": "51974",
        "ok": "113",
        "ko": "59997"
    },
    "standardDeviation": {
        "total": "20403",
        "ok": "101",
        "ko": "496"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "83",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "144",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "302",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "535",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2171,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 14040,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "18.829",
        "ok": "2.523",
        "ko": "16.307"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "2316",
        "ok": "316",
        "ko": "2000"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "15",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60007",
        "ok": "674",
        "ko": "60007"
    },
    "meanResponseTime": {
        "total": "51826",
        "ok": "90",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "20565",
        "ok": "82",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "69",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "103",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "246",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "411",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 316,
    "percentage": 14
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2000,
    "percentage": 86
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.367",
        "ko": "2.323"
    }
}
    },"req_getcartinproduc-4c391": {
        type: "REQUEST",
        name: "getCartInProductCatalogue",
path: "getCartInProductCatalogue",
pathFormatted: "req_getcartinproduc-4c391",
stats: {
    "name": "getCartInProductCatalogue",
    "numberOfRequests": {
        "total": "2316",
        "ok": "315",
        "ko": "2001"
    },
    "minResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "1174"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "788",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "51840",
        "ok": "187",
        "ko": "59971"
    },
    "standardDeviation": {
        "total": "20531",
        "ok": "140",
        "ko": "1315"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "152",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "241",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "520",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "653",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 315,
    "percentage": 14
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2001,
    "percentage": 86
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.366",
        "ko": "2.324"
    }
}
    },"req_addproduct-01-7f950": {
        type: "REQUEST",
        name: "addProduct_01",
path: "addProduct_01",
pathFormatted: "req_addproduct-01-7f950",
stats: {
    "name": "addProduct_01",
    "numberOfRequests": {
        "total": "2316",
        "ok": "311",
        "ko": "2005"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60022",
        "ok": "468",
        "ko": "60022"
    },
    "meanResponseTime": {
        "total": "51954",
        "ok": "79",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "20431",
        "ok": "73",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "55",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "105",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "221",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "391",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 311,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2005,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.361",
        "ko": "2.329"
    }
}
    },"req_addproduct-02-ca68d": {
        type: "REQUEST",
        name: "addProduct_02",
path: "addProduct_02",
pathFormatted: "req_addproduct-02-ca68d",
stats: {
    "name": "addProduct_02",
    "numberOfRequests": {
        "total": "2316",
        "ok": "310",
        "ko": "2006"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60023",
        "ok": "516",
        "ko": "60023"
    },
    "meanResponseTime": {
        "total": "51980",
        "ok": "74",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "20405",
        "ok": "68",
        "ko": "2"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "55",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "94",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "198",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "312",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 310,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2006,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.36",
        "ko": "2.33"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "2316",
        "ok": "309",
        "ko": "2007"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60019",
        "ok": "841",
        "ko": "60019"
    },
    "meanResponseTime": {
        "total": "52014",
        "ok": "136",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "20356",
        "ok": "97",
        "ko": "2"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "115",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "170",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "290",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "516",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 308,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2007,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.359",
        "ko": "2.331"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "2316",
        "ok": "306",
        "ko": "2010"
    },
    "minResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60025",
        "ok": "556",
        "ko": "60025"
    },
    "meanResponseTime": {
        "total": "52091",
        "ok": "134",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "20273",
        "ok": "91",
        "ko": "2"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "103",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "172",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "303",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60007",
        "ok": "487",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 306,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2010,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.355",
        "ko": "2.334"
    }
}
    },"req_checkoutgetcart-3cbad": {
        type: "REQUEST",
        name: "checkoutGetCartAfterEmpty",
path: "checkoutGetCartAfterEmpty",
pathFormatted: "req_checkoutgetcart-3cbad",
stats: {
    "name": "checkoutGetCartAfterEmpty",
    "numberOfRequests": {
        "total": "2316",
        "ok": "305",
        "ko": "2011"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60017",
        "ok": "741",
        "ko": "60017"
    },
    "meanResponseTime": {
        "total": "52111",
        "ok": "93",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "20258",
        "ok": "83",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "67",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "109",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "252",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "395",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2011,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.69",
        "ok": "0.354",
        "ko": "2.336"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
