var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4795",
        "ok": "4341",
        "ko": "454"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "1023"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "956",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "5636",
        "ok": "86",
        "ko": "58703"
    },
    "standardDeviation": {
        "total": "17367",
        "ok": "95",
        "ko": "8644"
    },
    "percentiles1": {
        "total": "63",
        "ok": "55",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "141",
        "ok": "108",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "266",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "479",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4337,
    "percentage": 90
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 454,
    "percentage": 9
},
    "meanNumberOfRequestsPerSecond": {
        "total": "10.052",
        "ok": "9.101",
        "ko": "0.952"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "959",
        "ok": "883",
        "ko": "76"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "603",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "4802",
        "ok": "52",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "16194",
        "ok": "55",
        "ko": "1"
    },
    "percentiles1": {
        "total": "38",
        "ok": "33",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "82",
        "ok": "68",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "143",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "246",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 883,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 76,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.01",
        "ok": "1.851",
        "ko": "0.159"
    }
}
    },"req_addproduct-d3929": {
        type: "REQUEST",
        name: "addProduct",
path: "addProduct",
pathFormatted: "req_addproduct-d3929",
stats: {
    "name": "addProduct",
    "numberOfRequests": {
        "total": "959",
        "ok": "878",
        "ko": "81"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "407",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "5116",
        "ok": "52",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "16671",
        "ok": "55",
        "ko": "1"
    },
    "percentiles1": {
        "total": "37",
        "ok": "30",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "93",
        "ok": "73",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "156",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "283",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 878,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 81,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.01",
        "ok": "1.841",
        "ko": "0.17"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "959",
        "ok": "874",
        "ko": "85"
    },
    "minResponseTime": {
        "total": "16",
        "ok": "16",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "700",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "5386",
        "ok": "74",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "17032",
        "ok": "73",
        "ko": "1"
    },
    "percentiles1": {
        "total": "52",
        "ok": "47",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "124",
        "ok": "95",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "206",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "341",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 874,
    "percentage": 91
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 85,
    "percentage": 9
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.01",
        "ok": "1.832",
        "ko": "0.178"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "959",
        "ok": "849",
        "ko": "110"
    },
    "minResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "1023"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "956",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "6411",
        "ok": "161",
        "ko": "54646"
    },
    "standardDeviation": {
        "total": "18285",
        "ok": "129",
        "ko": "16932"
    },
    "percentiles1": {
        "total": "131",
        "ok": "109",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "281",
        "ok": "218",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "412",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "625",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 846,
    "percentage": 88
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 110,
    "percentage": 11
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.01",
        "ok": "1.78",
        "ko": "0.231"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "959",
        "ok": "857",
        "ko": "102"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "927",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "6465",
        "ok": "94",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "18470",
        "ok": "97",
        "ko": "1"
    },
    "percentiles1": {
        "total": "69",
        "ok": "56",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "162",
        "ok": "128",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "254",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "517",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 856,
    "percentage": 89
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 102,
    "percentage": 11
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.01",
        "ok": "1.797",
        "ko": "0.214"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
