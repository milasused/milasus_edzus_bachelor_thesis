var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "7544",
        "ok": "5957",
        "ko": "1587"
    },
    "minResponseTime": {
        "total": "6",
        "ok": "6",
        "ko": "32"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "5596",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "1621",
        "ok": "630",
        "ko": "5343"
    },
    "standardDeviation": {
        "total": "6876",
        "ok": "971",
        "ko": "14271"
    },
    "percentiles1": {
        "total": "381",
        "ok": "220",
        "ko": "1579"
    },
    "percentiles2": {
        "total": "1383",
        "ok": "724",
        "ko": "2311"
    },
    "percentiles3": {
        "total": "3456",
        "ok": "3049",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "4410",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4624,
    "percentage": 61
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 348,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 985,
    "percentage": 13
},
    "group4": {
    "name": "failed",
    "count": 1587,
    "percentage": 21
},
    "meanNumberOfRequestsPerSecond": {
        "total": "23.429",
        "ok": "18.5",
        "ko": "4.929"
    }
},
contents: {
"req_authenticate-4dcaf": {
        type: "REQUEST",
        name: "authenticate",
path: "authenticate",
pathFormatted: "req_authenticate-4dcaf",
stats: {
    "name": "authenticate",
    "numberOfRequests": {
        "total": "1176",
        "ok": "1112",
        "ko": "64"
    },
    "minResponseTime": {
        "total": "102",
        "ok": "102",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "5596",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "4850",
        "ok": "1675",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "13318",
        "ok": "1565",
        "ko": "1"
    },
    "percentiles1": {
        "total": "1534",
        "ok": "1284",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "3412",
        "ok": "3041",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "4476",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "5081",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 509,
    "percentage": 43
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 38,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 565,
    "percentage": 48
},
    "group4": {
    "name": "failed",
    "count": 64,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.652",
        "ok": "3.453",
        "ko": "0.199"
    }
}
    },"req_createcart-6cb7c": {
        type: "REQUEST",
        name: "createCart",
path: "createCart",
pathFormatted: "req_createcart-6cb7c",
stats: {
    "name": "createCart",
    "numberOfRequests": {
        "total": "1112",
        "ok": "876",
        "ko": "236"
    },
    "minResponseTime": {
        "total": "6",
        "ok": "6",
        "ko": "1674"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "4192",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "1362",
        "ok": "568",
        "ko": "4310"
    },
    "standardDeviation": {
        "total": "4484",
        "ok": "837",
        "ko": "9006"
    },
    "percentiles1": {
        "total": "514",
        "ok": "162",
        "ko": "2789"
    },
    "percentiles2": {
        "total": "2301",
        "ok": "747",
        "ko": "3204"
    },
    "percentiles3": {
        "total": "3432",
        "ok": "2415",
        "ko": "3896"
    },
    "percentiles4": {
        "total": "4150",
        "ok": "3780",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 679,
    "percentage": 61
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 69,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 128,
    "percentage": 12
},
    "group4": {
    "name": "failed",
    "count": 236,
    "percentage": 21
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.453",
        "ok": "2.72",
        "ko": "0.733"
    }
}
    },"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "876",
        "ok": "867",
        "ko": "9"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "2430"
    },
    "maxResponseTime": {
        "total": "3086",
        "ok": "2617",
        "ko": "3086"
    },
    "meanResponseTime": {
        "total": "399",
        "ok": "375",
        "ko": "2735"
    },
    "standardDeviation": {
        "total": "475",
        "ok": "412",
        "ko": "202"
    },
    "percentiles1": {
        "total": "248",
        "ok": "244",
        "ko": "2692"
    },
    "percentiles2": {
        "total": "700",
        "ok": "689",
        "ko": "2851"
    },
    "percentiles3": {
        "total": "1031",
        "ok": "948",
        "ko": "3056"
    },
    "percentiles4": {
        "total": "2454",
        "ok": "1995",
        "ko": "3080"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 754,
    "percentage": 86
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 85,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 28,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 9,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.72",
        "ok": "2.693",
        "ko": "0.028"
    }
}
    },"req_addproduct-d3929": {
        type: "REQUEST",
        name: "addProduct",
path: "addProduct",
pathFormatted: "req_addproduct-d3929",
stats: {
    "name": "addProduct",
    "numberOfRequests": {
        "total": "876",
        "ok": "861",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "1182"
    },
    "maxResponseTime": {
        "total": "3197",
        "ok": "1965",
        "ko": "3197"
    },
    "meanResponseTime": {
        "total": "337",
        "ok": "298",
        "ko": "2523"
    },
    "standardDeviation": {
        "total": "430",
        "ok": "308",
        "ko": "684"
    },
    "percentiles1": {
        "total": "235",
        "ok": "228",
        "ko": "2829"
    },
    "percentiles2": {
        "total": "483",
        "ok": "468",
        "ko": "2985"
    },
    "percentiles3": {
        "total": "997",
        "ok": "811",
        "ko": "3107"
    },
    "percentiles4": {
        "total": "2672",
        "ok": "1495",
        "ko": "3179"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 816,
    "percentage": 93
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.72",
        "ok": "2.674",
        "ko": "0.047"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "876",
        "ok": "516",
        "ko": "360"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "32"
    },
    "maxResponseTime": {
        "total": "3634",
        "ok": "1324",
        "ko": "3634"
    },
    "meanResponseTime": {
        "total": "557",
        "ok": "199",
        "ko": "1070"
    },
    "standardDeviation": {
        "total": "615",
        "ok": "245",
        "ko": "621"
    },
    "percentiles1": {
        "total": "312",
        "ok": "92",
        "ko": "1014"
    },
    "percentiles2": {
        "total": "881",
        "ok": "260",
        "ko": "1492"
    },
    "percentiles3": {
        "total": "1729",
        "ok": "811",
        "ko": "2074"
    },
    "percentiles4": {
        "total": "2445",
        "ok": "1138",
        "ko": "3083"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 489,
    "percentage": 56
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 360,
    "percentage": 41
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.72",
        "ok": "1.602",
        "ko": "1.118"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "876",
        "ok": "446",
        "ko": "430"
    },
    "minResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "132"
    },
    "maxResponseTime": {
        "total": "3325",
        "ok": "1075",
        "ko": "3325"
    },
    "meanResponseTime": {
        "total": "861",
        "ok": "282",
        "ko": "1460"
    },
    "standardDeviation": {
        "total": "712",
        "ok": "256",
        "ko": "508"
    },
    "percentiles1": {
        "total": "715",
        "ok": "166",
        "ko": "1482"
    },
    "percentiles2": {
        "total": "1477",
        "ok": "425",
        "ko": "1783"
    },
    "percentiles3": {
        "total": "2117",
        "ok": "816",
        "ko": "2282"
    },
    "percentiles4": {
        "total": "2372",
        "ok": "988",
        "ko": "2564"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 419,
    "percentage": 48
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 430,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.72",
        "ok": "1.385",
        "ko": "1.335"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "876",
        "ok": "451",
        "ko": "425"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "207"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "1214",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "882",
        "ok": "179",
        "ko": "1629"
    },
    "standardDeviation": {
        "total": "2137",
        "ok": "211",
        "ko": "2877"
    },
    "percentiles1": {
        "total": "499",
        "ok": "86",
        "ko": "1560"
    },
    "percentiles2": {
        "total": "1547",
        "ok": "262",
        "ko": "1692"
    },
    "percentiles3": {
        "total": "2033",
        "ok": "570",
        "ko": "2261"
    },
    "percentiles4": {
        "total": "2760",
        "ok": "1043",
        "ko": "2932"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 439,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 425,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.72",
        "ok": "1.401",
        "ko": "1.32"
    }
}
    },"req_deletecart-5e30c": {
        type: "REQUEST",
        name: "deleteCart",
path: "deleteCart",
pathFormatted: "req_deletecart-5e30c",
stats: {
    "name": "deleteCart",
    "numberOfRequests": {
        "total": "876",
        "ok": "828",
        "ko": "48"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "1200"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "2965",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "2685",
        "ok": "602",
        "ko": "38629"
    },
    "standardDeviation": {
        "total": "10818",
        "ok": "663",
        "ko": "27593"
    },
    "percentiles1": {
        "total": "246",
        "ok": "186",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "1352",
        "ok": "1287",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "2747",
        "ok": "1688",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "1951",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 519,
    "percentage": 59
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 68,
    "percentage": 8
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 241,
    "percentage": 28
},
    "group4": {
    "name": "failed",
    "count": 48,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.72",
        "ok": "2.571",
        "ko": "0.149"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
