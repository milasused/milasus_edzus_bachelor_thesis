var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4360",
        "ok": "3882",
        "ko": "478"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "1046"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "1638",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "6010",
        "ok": "178",
        "ko": "53373"
    },
    "standardDeviation": {
        "total": "17722",
        "ok": "213",
        "ko": "18572"
    },
    "percentiles1": {
        "total": "122",
        "ok": "102",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "330",
        "ok": "229",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "637",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "968",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3778,
    "percentage": 87
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 88,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 16,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 478,
    "percentage": 11
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.971",
        "ok": "7.988",
        "ko": "0.984"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "872",
        "ok": "808",
        "ko": "64"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "1624",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "4514",
        "ok": "119",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "15617",
        "ok": "151",
        "ko": "1"
    },
    "percentiles1": {
        "total": "79",
        "ok": "67",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "178",
        "ok": "155",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "380",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "780",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 801,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 64,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.794",
        "ok": "1.663",
        "ko": "0.132"
    }
}
    },"req_addproduct-d3929": {
        type: "REQUEST",
        name: "addProduct",
path: "addProduct",
pathFormatted: "req_addproduct-d3929",
stats: {
    "name": "addProduct",
    "numberOfRequests": {
        "total": "872",
        "ok": "800",
        "ko": "72"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "861",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "5061",
        "ok": "116",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "16483",
        "ok": "143",
        "ko": "1"
    },
    "percentiles1": {
        "total": "76",
        "ok": "62",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "194",
        "ok": "155",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "423",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "702",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 798,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 72,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.794",
        "ok": "1.646",
        "ko": "0.148"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "872",
        "ok": "780",
        "ko": "92"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "1147"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "1638",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "5935",
        "ok": "159",
        "ko": "54905"
    },
    "standardDeviation": {
        "total": "17654",
        "ok": "210",
        "ko": "16512"
    },
    "percentiles1": {
        "total": "103",
        "ok": "80",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "272",
        "ok": "197",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "489",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "1035",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 761,
    "percentage": 87
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 92,
    "percentage": 11
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.794",
        "ok": "1.605",
        "ko": "0.189"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "872",
        "ok": "739",
        "ko": "133"
    },
    "minResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "1046"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "1462",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "7196",
        "ok": "314",
        "ko": "45432"
    },
    "standardDeviation": {
        "total": "19007",
        "ok": "267",
        "ko": "25361"
    },
    "percentiles1": {
        "total": "292",
        "ok": "207",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "690",
        "ok": "457",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "879",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "1002",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 682,
    "percentage": 78
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 53,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 133,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.794",
        "ok": "1.521",
        "ko": "0.274"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "872",
        "ok": "755",
        "ko": "117"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "1095"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "1630",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "7343",
        "ok": "192",
        "ko": "53491"
    },
    "standardDeviation": {
        "total": "19379",
        "ok": "214",
        "ko": "18413"
    },
    "percentiles1": {
        "total": "152",
        "ok": "117",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "378",
        "ok": "268",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "573",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "1074",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 736,
    "percentage": 84
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 117,
    "percentage": 13
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.794",
        "ok": "1.553",
        "ko": "0.241"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
