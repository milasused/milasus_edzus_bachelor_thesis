var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "11090",
        "ok": "589",
        "ko": "10501"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "1043"
    },
    "maxResponseTime": {
        "total": "60028",
        "ok": "972",
        "ko": "60028"
    },
    "meanResponseTime": {
        "total": "56800",
        "ok": "142",
        "ko": "59978"
    },
    "standardDeviation": {
        "total": "13465",
        "ok": "168",
        "ko": "1150"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "74",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "173",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "550",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "751",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 585,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 10501,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "14.347",
        "ok": "0.762",
        "ko": "13.585"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "2218",
        "ok": "121",
        "ko": "2097"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "15",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60016",
        "ok": "743",
        "ko": "60016"
    },
    "meanResponseTime": {
        "total": "56731",
        "ok": "66",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "13612",
        "ok": "81",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "42",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "72",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "171",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "339",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 121,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2097,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.869",
        "ok": "0.157",
        "ko": "2.713"
    }
}
    },"req_addproduct-d3929": {
        type: "REQUEST",
        name: "addProduct",
path: "addProduct",
pathFormatted: "req_addproduct-d3929",
stats: {
    "name": "addProduct",
    "numberOfRequests": {
        "total": "2218",
        "ok": "121",
        "ko": "2097"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60015",
        "ok": "549",
        "ko": "60015"
    },
    "meanResponseTime": {
        "total": "56731",
        "ok": "54",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "13614",
        "ok": "74",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "30",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "48",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "176",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "402",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 121,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2097,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.869",
        "ok": "0.157",
        "ko": "2.713"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "2218",
        "ok": "118",
        "ko": "2100"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60028",
        "ok": "550",
        "ko": "60028"
    },
    "meanResponseTime": {
        "total": "56814",
        "ok": "93",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "13445",
        "ok": "83",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "66",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "115",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "207",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "484",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 118,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2100,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.869",
        "ok": "0.153",
        "ko": "2.717"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "2218",
        "ok": "113",
        "ko": "2105"
    },
    "minResponseTime": {
        "total": "92",
        "ok": "92",
        "ko": "1043"
    },
    "maxResponseTime": {
        "total": "60014",
        "ok": "972",
        "ko": "60014"
    },
    "meanResponseTime": {
        "total": "56856",
        "ok": "352",
        "ko": "59889"
    },
    "standardDeviation": {
        "total": "13328",
        "ok": "214",
        "ko": "2567"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "273",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "481",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "753",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "959",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 109,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2105,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.869",
        "ok": "0.146",
        "ko": "2.723"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "2218",
        "ok": "116",
        "ko": "2102"
    },
    "minResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60017",
        "ok": "670",
        "ko": "60017"
    },
    "meanResponseTime": {
        "total": "56871",
        "ok": "160",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "13322",
        "ok": "139",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "122",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "199",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "465",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "665",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 116,
    "percentage": 5
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2102,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.869",
        "ok": "0.15",
        "ko": "2.719"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
