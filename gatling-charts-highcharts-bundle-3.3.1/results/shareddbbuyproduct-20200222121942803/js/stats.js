var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "5205",
        "ok": "2657",
        "ko": "2548"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "1016"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "982",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "29103",
        "ok": "115",
        "ko": "59330"
    },
    "standardDeviation": {
        "total": "29922",
        "ok": "122",
        "ko": "6250"
    },
    "percentiles1": {
        "total": "489",
        "ok": "76",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "145",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "353",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "642",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2651,
    "percentage": 51
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2548,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.278",
        "ok": "4.736",
        "ko": "4.542"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "1041",
        "ok": "542",
        "ko": "499"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "1024"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "788",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "28459",
        "ok": "73",
        "ko": "59292"
    },
    "standardDeviation": {
        "total": "29917",
        "ok": "78",
        "ko": "6420"
    },
    "percentiles1": {
        "total": "254",
        "ok": "47",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "89",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "205",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "372",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 542,
    "percentage": 52
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 499,
    "percentage": 48
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.856",
        "ok": "0.966",
        "ko": "0.889"
    }
}
    },"req_addproduct-d3929": {
        type: "REQUEST",
        name: "addProduct",
path: "addProduct",
pathFormatted: "req_addproduct-d3929",
stats: {
    "name": "addProduct",
    "numberOfRequests": {
        "total": "1041",
        "ok": "535",
        "ko": "506"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "1017"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "532",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "28856",
        "ok": "61",
        "ko": "59302"
    },
    "standardDeviation": {
        "total": "29941",
        "ok": "57",
        "ko": "6379"
    },
    "percentiles1": {
        "total": "211",
        "ok": "45",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "83",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "154",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "297",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 535,
    "percentage": 51
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 506,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.856",
        "ok": "0.954",
        "ko": "0.902"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "1041",
        "ok": "534",
        "ko": "507"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "18",
        "ko": "1019"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "941",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "28987",
        "ok": "94",
        "ko": "59420"
    },
    "standardDeviation": {
        "total": "29930",
        "ok": "97",
        "ko": "5821"
    },
    "percentiles1": {
        "total": "343",
        "ok": "66",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "117",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "229",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "497",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 532,
    "percentage": 51
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 507,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.856",
        "ok": "0.952",
        "ko": "0.904"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "1041",
        "ok": "524",
        "ko": "517"
    },
    "minResponseTime": {
        "total": "45",
        "ok": "45",
        "ko": "1016"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "842",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "29517",
        "ok": "229",
        "ko": "59202"
    },
    "standardDeviation": {
        "total": "29875",
        "ok": "163",
        "ko": "6813"
    },
    "percentiles1": {
        "total": "798",
        "ok": "182",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "297",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "585",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "780",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 522,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 517,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.856",
        "ok": "0.934",
        "ko": "0.922"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "1041",
        "ok": "522",
        "ko": "519"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "1062"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "982",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "29693",
        "ok": "123",
        "ko": "59433"
    },
    "standardDeviation": {
        "total": "29932",
        "ok": "106",
        "ko": "5751"
    },
    "percentiles1": {
        "total": "810",
        "ok": "93",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "163",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "316",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "419",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 520,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 519,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.856",
        "ok": "0.93",
        "ko": "0.925"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
