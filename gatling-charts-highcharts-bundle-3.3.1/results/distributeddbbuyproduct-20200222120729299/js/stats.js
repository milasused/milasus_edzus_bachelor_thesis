var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "9456",
        "ok": "7487",
        "ko": "1969"
    },
    "minResponseTime": {
        "total": "6",
        "ok": "6",
        "ko": "318"
    },
    "maxResponseTime": {
        "total": "3826",
        "ok": "3826",
        "ko": "3591"
    },
    "meanResponseTime": {
        "total": "755",
        "ok": "635",
        "ko": "1208"
    },
    "standardDeviation": {
        "total": "606",
        "ok": "609",
        "ko": "307"
    },
    "percentiles1": {
        "total": "740",
        "ok": "423",
        "ko": "1213"
    },
    "percentiles2": {
        "total": "1212",
        "ok": "1087",
        "ko": "1402"
    },
    "percentiles3": {
        "total": "1732",
        "ok": "1798",
        "ko": "1659"
    },
    "percentiles4": {
        "total": "2304",
        "ok": "2334",
        "ko": "1916"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4703,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1372,
    "percentage": 15
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1412,
    "percentage": 15
},
    "group4": {
    "name": "failed",
    "count": 1969,
    "percentage": 21
},
    "meanNumberOfRequestsPerSecond": {
        "total": "43.576",
        "ok": "34.502",
        "ko": "9.074"
    }
},
contents: {
"req_authenticate-4dcaf": {
        type: "REQUEST",
        name: "authenticate",
path: "authenticate",
pathFormatted: "req_authenticate-4dcaf",
stats: {
    "name": "authenticate",
    "numberOfRequests": {
        "total": "1182",
        "ok": "1182",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "109",
        "ok": "109",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1084",
        "ok": "1084",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "percentiles1": {
        "total": "708",
        "ok": "708",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1890",
        "ok": "1890",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2352",
        "ok": "2352",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 612,
    "percentage": 52
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 36,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 534,
    "percentage": 45
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "5.447",
        "ko": "-"
    }
}
    },"req_createcart-6cb7c": {
        type: "REQUEST",
        name: "createCart",
path: "createCart",
pathFormatted: "req_createcart-6cb7c",
stats: {
    "name": "createCart",
    "numberOfRequests": {
        "total": "1182",
        "ok": "1182",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6",
        "ok": "6",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2112",
        "ok": "2112",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "591",
        "ok": "591",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "515",
        "ok": "515",
        "ko": "-"
    },
    "percentiles1": {
        "total": "466",
        "ok": "466",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1071",
        "ok": "1071",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1394",
        "ok": "1394",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1632",
        "ok": "1632",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 695,
    "percentage": 59
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 302,
    "percentage": 26
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 185,
    "percentage": 16
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "5.447",
        "ko": "-"
    }
}
    },"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "1182",
        "ok": "1182",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2805",
        "ok": "2805",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "638",
        "ok": "638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "530",
        "ok": "530",
        "ko": "-"
    },
    "percentiles1": {
        "total": "505",
        "ok": "505",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1111",
        "ok": "1111",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1462",
        "ok": "1462",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1735",
        "ok": "1735",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 684,
    "percentage": 58
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 277,
    "percentage": 23
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 221,
    "percentage": 19
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "5.447",
        "ko": "-"
    }
}
    },"req_addproduct-d3929": {
        type: "REQUEST",
        name: "addProduct",
path: "addProduct",
pathFormatted: "req_addproduct-d3929",
stats: {
    "name": "addProduct",
    "numberOfRequests": {
        "total": "1182",
        "ok": "1182",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2656",
        "ok": "2656",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "650",
        "ok": "650",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "531",
        "ok": "531",
        "ko": "-"
    },
    "percentiles1": {
        "total": "542",
        "ok": "542",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1124",
        "ok": "1124",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1483",
        "ok": "1483",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 681,
    "percentage": 58
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 275,
    "percentage": 23
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 226,
    "percentage": 19
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "5.447",
        "ko": "-"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "1182",
        "ok": "591",
        "ko": "591"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "342"
    },
    "maxResponseTime": {
        "total": "2461",
        "ok": "1703",
        "ko": "2461"
    },
    "meanResponseTime": {
        "total": "789",
        "ok": "366",
        "ko": "1211"
    },
    "standardDeviation": {
        "total": "554",
        "ok": "401",
        "ko": "311"
    },
    "percentiles1": {
        "total": "896",
        "ok": "162",
        "ko": "1225"
    },
    "percentiles2": {
        "total": "1259",
        "ok": "619",
        "ko": "1426"
    },
    "percentiles3": {
        "total": "1586",
        "ok": "1214",
        "ko": "1665"
    },
    "percentiles4": {
        "total": "1774",
        "ok": "1398",
        "ko": "1843"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 489,
    "percentage": 41
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 70,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 32,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 591,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "2.724",
        "ko": "2.724"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "1182",
        "ok": "384",
        "ko": "798"
    },
    "minResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "338"
    },
    "maxResponseTime": {
        "total": "3591",
        "ok": "1397",
        "ko": "3591"
    },
    "meanResponseTime": {
        "total": "904",
        "ok": "279",
        "ko": "1205"
    },
    "standardDeviation": {
        "total": "520",
        "ok": "289",
        "ko": "286"
    },
    "percentiles1": {
        "total": "1085",
        "ok": "147",
        "ko": "1209"
    },
    "percentiles2": {
        "total": "1290",
        "ok": "330",
        "ko": "1388"
    },
    "percentiles3": {
        "total": "1557",
        "ok": "955",
        "ko": "1584"
    },
    "percentiles4": {
        "total": "1795",
        "ok": "1107",
        "ko": "1852"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 346,
    "percentage": 29
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 798,
    "percentage": 68
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "1.77",
        "ko": "3.677"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "1182",
        "ok": "602",
        "ko": "580"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "318"
    },
    "maxResponseTime": {
        "total": "2691",
        "ok": "1488",
        "ko": "2691"
    },
    "meanResponseTime": {
        "total": "804",
        "ok": "414",
        "ko": "1210"
    },
    "standardDeviation": {
        "total": "547",
        "ok": "413",
        "ko": "330"
    },
    "percentiles1": {
        "total": "876",
        "ok": "199",
        "ko": "1209"
    },
    "percentiles2": {
        "total": "1241",
        "ok": "752",
        "ko": "1401"
    },
    "percentiles3": {
        "total": "1592",
        "ok": "1213",
        "ko": "1717"
    },
    "percentiles4": {
        "total": "1869",
        "ok": "1394",
        "ko": "2109"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 466,
    "percentage": 39
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 102,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 34,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 580,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "2.774",
        "ko": "2.673"
    }
}
    },"req_deletecart-5e30c": {
        type: "REQUEST",
        name: "deleteCart",
path: "deleteCart",
pathFormatted: "req_deletecart-5e30c",
stats: {
    "name": "deleteCart",
    "numberOfRequests": {
        "total": "1182",
        "ok": "1182",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "577",
        "ok": "577",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "520",
        "ok": "520",
        "ko": "-"
    },
    "percentiles1": {
        "total": "404",
        "ok": "404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1056",
        "ok": "1056",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1437",
        "ok": "1437",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1605",
        "ok": "1605",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 730,
    "percentage": 62
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 275,
    "percentage": 23
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 177,
    "percentage": 15
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.447",
        "ok": "5.447",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
