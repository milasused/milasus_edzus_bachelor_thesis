var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "50292",
        "ok": "5493",
        "ko": "44799"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "105"
    },
    "maxResponseTime": {
        "total": "68597",
        "ok": "3731",
        "ko": "68597"
    },
    "meanResponseTime": {
        "total": "34208",
        "ok": "483",
        "ko": "38343"
    },
    "standardDeviation": {
        "total": "25462",
        "ok": "542",
        "ko": "23900"
    },
    "percentiles1": {
        "total": "22243",
        "ok": "238",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "747",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60882",
        "ok": "1591",
        "ko": "61124"
    },
    "percentiles4": {
        "total": "63773",
        "ok": "2175",
        "ko": "63972"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4181,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 626,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 686,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 44799,
    "percentage": 89
},
    "meanNumberOfRequestsPerSecond": {
        "total": "28.624",
        "ok": "3.126",
        "ko": "25.497"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "2286",
        "ok": "355",
        "ko": "1931"
    },
    "minResponseTime": {
        "total": "14",
        "ok": "14",
        "ko": "1348"
    },
    "maxResponseTime": {
        "total": "60017",
        "ok": "2454",
        "ko": "60017"
    },
    "meanResponseTime": {
        "total": "50450",
        "ok": "616",
        "ko": "59612"
    },
    "standardDeviation": {
        "total": "21805",
        "ok": "627",
        "ko": "4724"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "324",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "1059",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "1845",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "2267",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 238,
    "percentage": 10
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 44,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 73,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 1931,
    "percentage": 84
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.202",
        "ko": "1.099"
    }
}
    },"req_cataloguegetcar-33446": {
        type: "REQUEST",
        name: "catalogueGetCart",
path: "catalogueGetCart",
pathFormatted: "req_cataloguegetcar-33446",
stats: {
    "name": "catalogueGetCart",
    "numberOfRequests": {
        "total": "2286",
        "ok": "271",
        "ko": "2015"
    },
    "minResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "1136"
    },
    "maxResponseTime": {
        "total": "60013",
        "ok": "2820",
        "ko": "60013"
    },
    "meanResponseTime": {
        "total": "50676",
        "ok": "635",
        "ko": "57406"
    },
    "standardDeviation": {
        "total": "21536",
        "ok": "574",
        "ko": "12002"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "451",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "957",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "1726",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "2308",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 189,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 33,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 49,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 2015,
    "percentage": 88
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.154",
        "ko": "1.147"
    }
}
    },"req_addproduct01-5908a": {
        type: "REQUEST",
        name: "addProduct01",
path: "addProduct01",
pathFormatted: "req_addproduct01-5908a",
stats: {
    "name": "addProduct01",
    "numberOfRequests": {
        "total": "2286",
        "ok": "346",
        "ko": "1940"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "1340"
    },
    "maxResponseTime": {
        "total": "60021",
        "ok": "3731",
        "ko": "60021"
    },
    "meanResponseTime": {
        "total": "50679",
        "ok": "586",
        "ko": "59613"
    },
    "standardDeviation": {
        "total": "21599",
        "ok": "646",
        "ko": "4722"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "296",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "1054",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "1812",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "2563",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 236,
    "percentage": 10
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 51,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 59,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 1940,
    "percentage": 85
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.197",
        "ko": "1.104"
    }
}
    },"req_addproduct02-9aec3": {
        type: "REQUEST",
        name: "addProduct02",
path: "addProduct02",
pathFormatted: "req_addproduct02-9aec3",
stats: {
    "name": "addProduct02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "336",
        "ko": "1950"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "1404"
    },
    "maxResponseTime": {
        "total": "60021",
        "ok": "3048",
        "ko": "60021"
    },
    "meanResponseTime": {
        "total": "50954",
        "ok": "519",
        "ko": "59644"
    },
    "standardDeviation": {
        "total": "21352",
        "ok": "545",
        "ko": "4537"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "299",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "931",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "1497",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "1917",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 242,
    "percentage": 11
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 50,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 44,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 1950,
    "percentage": 85
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.191",
        "ko": "1.11"
    }
}
    },"req_addproduct03-4138d": {
        type: "REQUEST",
        name: "addProduct03",
path: "addProduct03",
pathFormatted: "req_addproduct03-4138d",
stats: {
    "name": "addProduct03",
    "numberOfRequests": {
        "total": "2286",
        "ok": "338",
        "ko": "1948"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "1539"
    },
    "maxResponseTime": {
        "total": "60081",
        "ok": "3269",
        "ko": "60081"
    },
    "meanResponseTime": {
        "total": "50953",
        "ok": "515",
        "ko": "59704"
    },
    "standardDeviation": {
        "total": "21355",
        "ok": "555",
        "ko": "4140"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "295",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "911",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "1563",
        "ko": "60006"
    },
    "percentiles4": {
        "total": "60012",
        "ok": "2210",
        "ko": "60012"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 241,
    "percentage": 11
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 58,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 39,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 1948,
    "percentage": 85
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.192",
        "ko": "1.109"
    }
}
    },"req_gotocartgetcart-a23b9": {
        type: "REQUEST",
        name: "goToCartGetCarts",
path: "goToCartGetCarts",
pathFormatted: "req_gotocartgetcart-a23b9",
stats: {
    "name": "goToCartGetCarts",
    "numberOfRequests": {
        "total": "2286",
        "ok": "280",
        "ko": "2006"
    },
    "minResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "1187"
    },
    "maxResponseTime": {
        "total": "60101",
        "ok": "3434",
        "ko": "60101"
    },
    "meanResponseTime": {
        "total": "51105",
        "ok": "678",
        "ko": "58144"
    },
    "standardDeviation": {
        "total": "21141",
        "ok": "620",
        "ko": "10237"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "518",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "1014",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "1786",
        "ko": "60007"
    },
    "percentiles4": {
        "total": "60013",
        "ok": "2449",
        "ko": "60013"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 185,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 35,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 60,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 2006,
    "percentage": 88
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.159",
        "ko": "1.142"
    }
}
    },"req_cartsgetcartpro-e1bbc": {
        type: "REQUEST",
        name: "cartsGetCartProducts",
path: "cartsGetCartProducts",
pathFormatted: "req_cartsgetcartpro-e1bbc",
stats: {
    "name": "cartsGetCartProducts",
    "numberOfRequests": {
        "total": "2286",
        "ok": "336",
        "ko": "1950"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "1676"
    },
    "maxResponseTime": {
        "total": "60219",
        "ok": "3134",
        "ko": "60219"
    },
    "meanResponseTime": {
        "total": "51101",
        "ok": "476",
        "ko": "59824"
    },
    "standardDeviation": {
        "total": "21224",
        "ok": "543",
        "ko": "3213"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "194",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "834",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "1493",
        "ko": "60011"
    },
    "percentiles4": {
        "total": "60023",
        "ok": "2062",
        "ko": "60026"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 241,
    "percentage": 11
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 57,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 38,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 1950,
    "percentage": 85
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.191",
        "ko": "1.11"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "2286",
        "ok": "190",
        "ko": "2096"
    },
    "minResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "204"
    },
    "maxResponseTime": {
        "total": "63169",
        "ok": "1134",
        "ko": "63169"
    },
    "meanResponseTime": {
        "total": "51223",
        "ok": "323",
        "ko": "55837"
    },
    "standardDeviation": {
        "total": "21093",
        "ok": "297",
        "ko": "15136"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "214",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "442",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60015",
        "ok": "1015",
        "ko": "60016"
    },
    "percentiles4": {
        "total": "60036",
        "ok": "1110",
        "ko": "60039"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 166,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 24,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2096,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.108",
        "ko": "1.193"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "2286",
        "ok": "149",
        "ko": "2137"
    },
    "minResponseTime": {
        "total": "90",
        "ok": "90",
        "ko": "188"
    },
    "maxResponseTime": {
        "total": "64992",
        "ok": "1009",
        "ko": "64992"
    },
    "meanResponseTime": {
        "total": "50559",
        "ok": "390",
        "ko": "54057"
    },
    "standardDeviation": {
        "total": "22219",
        "ok": "251",
        "ko": "18449"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "293",
        "ko": "60006"
    },
    "percentiles2": {
        "total": "60303",
        "ok": "552",
        "ko": "60449"
    },
    "percentiles3": {
        "total": "62392",
        "ok": "861",
        "ko": "62448"
    },
    "percentiles4": {
        "total": "63568",
        "ok": "990",
        "ko": "63614"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 130,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2137,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.085",
        "ko": "1.216"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "2286",
        "ok": "174",
        "ko": "2112"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "119"
    },
    "maxResponseTime": {
        "total": "68097",
        "ok": "3684",
        "ko": "68097"
    },
    "meanResponseTime": {
        "total": "37477",
        "ok": "268",
        "ko": "40543"
    },
    "standardDeviation": {
        "total": "28085",
        "ok": "391",
        "ko": "27023"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "138",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60115",
        "ok": "345",
        "ko": "60365"
    },
    "percentiles3": {
        "total": "63538",
        "ok": "833",
        "ko": "63725"
    },
    "percentiles4": {
        "total": "65614",
        "ok": "1523",
        "ko": "65930"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 163,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2112,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.099",
        "ko": "1.202"
    }
}
    },"req_getcartafteremp-3ee9d": {
        type: "REQUEST",
        name: "getCartAfterEmptying",
path: "getCartAfterEmptying",
pathFormatted: "req_getcartafteremp-3ee9d",
stats: {
    "name": "getCartAfterEmptying",
    "numberOfRequests": {
        "total": "2286",
        "ok": "170",
        "ko": "2116"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "145"
    },
    "maxResponseTime": {
        "total": "67047",
        "ok": "2268",
        "ko": "67047"
    },
    "meanResponseTime": {
        "total": "29372",
        "ok": "141",
        "ko": "31721"
    },
    "standardDeviation": {
        "total": "26996",
        "ok": "245",
        "ko": "26706"
    },
    "percentiles1": {
        "total": "15515",
        "ok": "70",
        "ko": "17600"
    },
    "percentiles2": {
        "total": "60174",
        "ok": "132",
        "ko": "60402"
    },
    "percentiles3": {
        "total": "63721",
        "ok": "439",
        "ko": "63893"
    },
    "percentiles4": {
        "total": "65505",
        "ok": "1178",
        "ko": "65554"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 164,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2116,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.097",
        "ko": "1.204"
    }
}
    },"req_gotoproductcata-3c842": {
        type: "REQUEST",
        name: "goToProductCatalogue02",
path: "goToProductCatalogue02",
pathFormatted: "req_gotoproductcata-3c842",
stats: {
    "name": "goToProductCatalogue02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "306",
        "ko": "1980"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "1387"
    },
    "maxResponseTime": {
        "total": "67047",
        "ok": "2972",
        "ko": "67047"
    },
    "meanResponseTime": {
        "total": "21964",
        "ok": "627",
        "ko": "25261"
    },
    "standardDeviation": {
        "total": "22674",
        "ok": "656",
        "ko": "22633"
    },
    "percentiles1": {
        "total": "14517",
        "ok": "302",
        "ko": "15890"
    },
    "percentiles2": {
        "total": "23484",
        "ok": "1110",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "63253",
        "ok": "1922",
        "ko": "63453"
    },
    "percentiles4": {
        "total": "65214",
        "ok": "2327",
        "ko": "65332"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 196,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 44,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 66,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 1980,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.174",
        "ko": "1.127"
    }
}
    },"req_cataloguegetcar-eeca7": {
        type: "REQUEST",
        name: "catalogueGetCart02",
path: "catalogueGetCart02",
pathFormatted: "req_cataloguegetcar-eeca7",
stats: {
    "name": "catalogueGetCart02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "239",
        "ko": "2047"
    },
    "minResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "1247"
    },
    "maxResponseTime": {
        "total": "67310",
        "ok": "2336",
        "ko": "67310"
    },
    "meanResponseTime": {
        "total": "19512",
        "ok": "611",
        "ko": "21719"
    },
    "standardDeviation": {
        "total": "19134",
        "ok": "583",
        "ko": "19032"
    },
    "percentiles1": {
        "total": "15718",
        "ok": "394",
        "ko": "16514"
    },
    "percentiles2": {
        "total": "20900",
        "ok": "989",
        "ko": "21570"
    },
    "percentiles3": {
        "total": "62887",
        "ok": "1676",
        "ko": "63052"
    },
    "percentiles4": {
        "total": "65095",
        "ok": "2145",
        "ko": "65193"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 168,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 50,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 2047,
    "percentage": 90
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.136",
        "ko": "1.165"
    }
}
    },"req_addproduct04-c4b8c": {
        type: "REQUEST",
        name: "addProduct04",
path: "addProduct04",
pathFormatted: "req_addproduct04-c4b8c",
stats: {
    "name": "addProduct04",
    "numberOfRequests": {
        "total": "2286",
        "ok": "300",
        "ko": "1986"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "1493"
    },
    "maxResponseTime": {
        "total": "68597",
        "ok": "3045",
        "ko": "68597"
    },
    "meanResponseTime": {
        "total": "18377",
        "ok": "578",
        "ko": "21065"
    },
    "standardDeviation": {
        "total": "16213",
        "ok": "621",
        "ko": "15730"
    },
    "percentiles1": {
        "total": "17204",
        "ok": "265",
        "ko": "18113"
    },
    "percentiles2": {
        "total": "21133",
        "ok": "1048",
        "ko": "21870"
    },
    "percentiles3": {
        "total": "61572",
        "ok": "1690",
        "ko": "62215"
    },
    "percentiles4": {
        "total": "64268",
        "ok": "2278",
        "ko": "64332"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 194,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 42,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 64,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 1986,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.171",
        "ko": "1.13"
    }
}
    },"req_addproduct05-ae61d": {
        type: "REQUEST",
        name: "addProduct05",
path: "addProduct05",
pathFormatted: "req_addproduct05-ae61d",
stats: {
    "name": "addProduct05",
    "numberOfRequests": {
        "total": "2286",
        "ok": "289",
        "ko": "1997"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "1412"
    },
    "maxResponseTime": {
        "total": "68597",
        "ok": "3355",
        "ko": "68597"
    },
    "meanResponseTime": {
        "total": "18747",
        "ok": "490",
        "ko": "21389"
    },
    "standardDeviation": {
        "total": "15277",
        "ok": "552",
        "ko": "14557"
    },
    "percentiles1": {
        "total": "18272",
        "ok": "231",
        "ko": "19202"
    },
    "percentiles2": {
        "total": "21748",
        "ok": "801",
        "ko": "22200"
    },
    "percentiles3": {
        "total": "60011",
        "ok": "1501",
        "ko": "60192"
    },
    "percentiles4": {
        "total": "64014",
        "ok": "1998",
        "ko": "64108"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 216,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 34,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 39,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 1997,
    "percentage": 87
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.164",
        "ko": "1.137"
    }
}
    },"req_addproduct06-5e44a": {
        type: "REQUEST",
        name: "addProduct06",
path: "addProduct06",
pathFormatted: "req_addproduct06-5e44a",
stats: {
    "name": "addProduct06",
    "numberOfRequests": {
        "total": "2286",
        "ok": "282",
        "ko": "2004"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "2117"
    },
    "maxResponseTime": {
        "total": "67310",
        "ok": "2316",
        "ko": "67310"
    },
    "meanResponseTime": {
        "total": "18744",
        "ok": "444",
        "ko": "21319"
    },
    "standardDeviation": {
        "total": "14885",
        "ok": "501",
        "ko": "14105"
    },
    "percentiles1": {
        "total": "18175",
        "ok": "208",
        "ko": "18901"
    },
    "percentiles2": {
        "total": "20992",
        "ok": "637",
        "ko": "21644"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "1454",
        "ko": "60014"
    },
    "percentiles4": {
        "total": "63637",
        "ok": "2025",
        "ko": "64099"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 220,
    "percentage": 10
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 32,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 30,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 2004,
    "percentage": 88
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.161",
        "ko": "1.141"
    }
}
    },"req_gotocartgetcart-45df1": {
        type: "REQUEST",
        name: "goToCartGetCarts02",
path: "goToCartGetCarts02",
pathFormatted: "req_gotocartgetcart-45df1",
stats: {
    "name": "goToCartGetCarts02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "228",
        "ko": "2058"
    },
    "minResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "1128"
    },
    "maxResponseTime": {
        "total": "66810",
        "ok": "1972",
        "ko": "66810"
    },
    "meanResponseTime": {
        "total": "17937",
        "ok": "516",
        "ko": "19867"
    },
    "standardDeviation": {
        "total": "14113",
        "ok": "489",
        "ko": "13560"
    },
    "percentiles1": {
        "total": "17214",
        "ok": "357",
        "ko": "17717"
    },
    "percentiles2": {
        "total": "19845",
        "ok": "680",
        "ko": "20387"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "1552",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "62313",
        "ok": "1739",
        "ko": "62434"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 179,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 30,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 2058,
    "percentage": 90
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.13",
        "ko": "1.171"
    }
}
    },"req_cartsgetcartpro-c42e3": {
        type: "REQUEST",
        name: "cartsGetCartProducts02",
path: "cartsGetCartProducts02",
pathFormatted: "req_cartsgetcartpro-c42e3",
stats: {
    "name": "cartsGetCartProducts02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "279",
        "ko": "2007"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "1780"
    },
    "maxResponseTime": {
        "total": "67310",
        "ok": "2798",
        "ko": "67310"
    },
    "meanResponseTime": {
        "total": "18207",
        "ok": "427",
        "ko": "20678"
    },
    "standardDeviation": {
        "total": "15289",
        "ok": "522",
        "ko": "14702"
    },
    "percentiles1": {
        "total": "16491",
        "ok": "120",
        "ko": "17008"
    },
    "percentiles2": {
        "total": "19192",
        "ok": "724",
        "ko": "19474"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "1409",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "61370",
        "ok": "1782",
        "ko": "61492"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 215,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 37,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 2007,
    "percentage": 88
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.159",
        "ko": "1.142"
    }
}
    },"req_gotocheckout02-fc475": {
        type: "REQUEST",
        name: "goToCheckout02",
path: "goToCheckout02",
pathFormatted: "req_gotocheckout02-fc475",
stats: {
    "name": "goToCheckout02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "165",
        "ko": "2121"
    },
    "minResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "135"
    },
    "maxResponseTime": {
        "total": "66621",
        "ok": "1152",
        "ko": "66621"
    },
    "meanResponseTime": {
        "total": "18823",
        "ok": "247",
        "ko": "20269"
    },
    "standardDeviation": {
        "total": "17054",
        "ok": "225",
        "ko": "16868"
    },
    "percentiles1": {
        "total": "15534",
        "ok": "166",
        "ko": "15869"
    },
    "percentiles2": {
        "total": "18339",
        "ok": "357",
        "ko": "18716"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "732",
        "ko": "60007"
    },
    "percentiles4": {
        "total": "61466",
        "ok": "938",
        "ko": "61501"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 162,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2121,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.094",
        "ko": "1.207"
    }
}
    },"req_placeorder02-b9d52": {
        type: "REQUEST",
        name: "placeOrder02",
path: "placeOrder02",
pathFormatted: "req_placeorder02-b9d52",
stats: {
    "name": "placeOrder02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "145",
        "ko": "2141"
    },
    "minResponseTime": {
        "total": "82",
        "ok": "82",
        "ko": "165"
    },
    "maxResponseTime": {
        "total": "65967",
        "ok": "951",
        "ko": "65967"
    },
    "meanResponseTime": {
        "total": "20863",
        "ok": "426",
        "ko": "22247"
    },
    "standardDeviation": {
        "total": "19731",
        "ok": "234",
        "ko": "19633"
    },
    "percentiles1": {
        "total": "14986",
        "ok": "363",
        "ko": "15229"
    },
    "percentiles2": {
        "total": "18340",
        "ok": "552",
        "ko": "19272"
    },
    "percentiles3": {
        "total": "60054",
        "ok": "884",
        "ko": "60104"
    },
    "percentiles4": {
        "total": "61303",
        "ok": "942",
        "ko": "61306"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 128,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2141,
    "percentage": 94
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.083",
        "ko": "1.219"
    }
}
    },"req_emptycart02-536c6": {
        type: "REQUEST",
        name: "emptyCart02",
path: "emptyCart02",
pathFormatted: "req_emptycart02-536c6",
stats: {
    "name": "emptyCart02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "157",
        "ko": "2129"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "115"
    },
    "maxResponseTime": {
        "total": "65336",
        "ok": "1350",
        "ko": "65336"
    },
    "meanResponseTime": {
        "total": "24595",
        "ok": "208",
        "ko": "26393"
    },
    "standardDeviation": {
        "total": "23260",
        "ok": "201",
        "ko": "23105"
    },
    "percentiles1": {
        "total": "14704",
        "ok": "131",
        "ko": "15093"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "293",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60281",
        "ok": "601",
        "ko": "60393"
    },
    "percentiles4": {
        "total": "61396",
        "ok": "845",
        "ko": "61431"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 155,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2129,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.089",
        "ko": "1.212"
    }
}
    },"req_getcartafteremp-44c42": {
        type: "REQUEST",
        name: "getCartAfterEmptying02",
path: "getCartAfterEmptying02",
pathFormatted: "req_getcartafteremp-44c42",
stats: {
    "name": "getCartAfterEmptying02",
    "numberOfRequests": {
        "total": "2286",
        "ok": "158",
        "ko": "2128"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "105"
    },
    "maxResponseTime": {
        "total": "62535",
        "ok": "1334",
        "ko": "62535"
    },
    "meanResponseTime": {
        "total": "30247",
        "ok": "146",
        "ko": "32481"
    },
    "standardDeviation": {
        "total": "25929",
        "ok": "215",
        "ko": "25494"
    },
    "percentiles1": {
        "total": "15629",
        "ok": "74",
        "ko": "17221"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "119",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60621",
        "ok": "559",
        "ko": "60733"
    },
    "percentiles4": {
        "total": "61507",
        "ok": "1098",
        "ko": "61545"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 153,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2128,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.301",
        "ok": "0.09",
        "ko": "1.211"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
