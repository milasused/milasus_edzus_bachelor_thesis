var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "25992",
        "ok": "1985",
        "ko": "24007"
    },
    "minResponseTime": {
        "total": "6",
        "ok": "6",
        "ko": "1041"
    },
    "maxResponseTime": {
        "total": "62128",
        "ok": "1008",
        "ko": "62128"
    },
    "meanResponseTime": {
        "total": "53320",
        "ok": "102",
        "ko": "57720"
    },
    "standardDeviation": {
        "total": "18702",
        "ok": "113",
        "ko": "11187"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "59",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "128",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60018",
        "ok": "332",
        "ko": "60020"
    },
    "percentiles4": {
        "total": "60522",
        "ok": "518",
        "ko": "60565"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1980,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 24007,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "22.368",
        "ok": "1.708",
        "ko": "20.66"
    }
},
contents: {
"req_gotoproductcata-a788e": {
        type: "REQUEST",
        name: "goToProductCatalogue",
path: "goToProductCatalogue",
pathFormatted: "req_gotoproductcata-a788e",
stats: {
    "name": "goToProductCatalogue",
    "numberOfRequests": {
        "total": "2166",
        "ok": "172",
        "ko": "1994"
    },
    "minResponseTime": {
        "total": "14",
        "ok": "14",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60021",
        "ok": "408",
        "ko": "60021"
    },
    "meanResponseTime": {
        "total": "55242",
        "ok": "68",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "16204",
        "ok": "57",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "48",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "91",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "172",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "262",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 172,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1994,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.148",
        "ko": "1.716"
    }
}
    },"req_getcartsinprodu-c103d": {
        type: "REQUEST",
        name: "getCartsInProductCatalogue",
path: "getCartsInProductCatalogue",
pathFormatted: "req_getcartsinprodu-c103d",
stats: {
    "name": "getCartsInProductCatalogue",
    "numberOfRequests": {
        "total": "2166",
        "ok": "169",
        "ko": "1997"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60011",
        "ok": "509",
        "ko": "60011"
    },
    "meanResponseTime": {
        "total": "55329",
        "ok": "131",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "16058",
        "ok": "84",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "105",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "156",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "309",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "389",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 169,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1997,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.145",
        "ko": "1.719"
    }
}
    },"req_addproducttocar-ab114": {
        type: "REQUEST",
        name: "addProductToCart01",
path: "addProductToCart01",
pathFormatted: "req_addproducttocar-ab114",
stats: {
    "name": "addProductToCart01",
    "numberOfRequests": {
        "total": "2166",
        "ok": "167",
        "ko": "1999"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60027",
        "ok": "234",
        "ko": "60027"
    },
    "meanResponseTime": {
        "total": "55379",
        "ok": "51",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "15992",
        "ok": "47",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "34",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "61",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "166",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "212",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 167,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1999,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.144",
        "ko": "1.72"
    }
}
    },"req_addproducttocar-e70b3": {
        type: "REQUEST",
        name: "addProductToCart02",
path: "addProductToCart02",
pathFormatted: "req_addproducttocar-e70b3",
stats: {
    "name": "addProductToCart02",
    "numberOfRequests": {
        "total": "2166",
        "ok": "167",
        "ko": "1999"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60020",
        "ok": "468",
        "ko": "60020"
    },
    "meanResponseTime": {
        "total": "55379",
        "ok": "52",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "15991",
        "ok": "57",
        "ko": "2"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "37",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "63",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "133",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60010",
        "ok": "274",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 167,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1999,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.144",
        "ko": "1.72"
    }
}
    },"req_addproducttocar-aec17": {
        type: "REQUEST",
        name: "addProductToCart03",
path: "addProductToCart03",
pathFormatted: "req_addproducttocar-aec17",
stats: {
    "name": "addProductToCart03",
    "numberOfRequests": {
        "total": "2166",
        "ok": "167",
        "ko": "1999"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60081",
        "ok": "534",
        "ko": "60081"
    },
    "meanResponseTime": {
        "total": "55380",
        "ok": "56",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "15991",
        "ok": "64",
        "ko": "3"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "35",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "59",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "159",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60013",
        "ok": "289",
        "ko": "60013"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 167,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1999,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.144",
        "ko": "1.72"
    }
}
    },"req_addproducttocar-7b724": {
        type: "REQUEST",
        name: "addProductToCart04",
path: "addProductToCart04",
pathFormatted: "req_addproducttocar-7b724",
stats: {
    "name": "addProductToCart04",
    "numberOfRequests": {
        "total": "2166",
        "ok": "166",
        "ko": "2000"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60059",
        "ok": "336",
        "ko": "60059"
    },
    "meanResponseTime": {
        "total": "55408",
        "ok": "53",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "15948",
        "ok": "51",
        "ko": "4"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "36",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "63",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "150",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60017",
        "ok": "234",
        "ko": "60018"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 166,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2000,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.143",
        "ko": "1.721"
    }
}
    },"req_gotocartgetcart-a23b9": {
        type: "REQUEST",
        name: "goToCartGetCarts",
path: "goToCartGetCarts",
pathFormatted: "req_gotocartgetcart-a23b9",
stats: {
    "name": "goToCartGetCarts",
    "numberOfRequests": {
        "total": "2166",
        "ok": "166",
        "ko": "2000"
    },
    "minResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60236",
        "ok": "756",
        "ko": "60236"
    },
    "meanResponseTime": {
        "total": "55416",
        "ok": "140",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "15925",
        "ok": "114",
        "ko": "8"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "101",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "185",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60017",
        "ok": "361",
        "ko": "60017"
    },
    "percentiles4": {
        "total": "60034",
        "ok": "505",
        "ko": "60035"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 166,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2000,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.143",
        "ko": "1.721"
    }
}
    },"req_cartsgetcartpro-e1bbc": {
        type: "REQUEST",
        name: "cartsGetCartProducts",
path: "cartsGetCartProducts",
pathFormatted: "req_cartsgetcartpro-e1bbc",
stats: {
    "name": "cartsGetCartProducts",
    "numberOfRequests": {
        "total": "2166",
        "ok": "164",
        "ko": "2002"
    },
    "minResponseTime": {
        "total": "6",
        "ok": "6",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "62128",
        "ok": "215",
        "ko": "62128"
    },
    "meanResponseTime": {
        "total": "55527",
        "ok": "33",
        "ko": "60073"
    },
    "standardDeviation": {
        "total": "15885",
        "ok": "31",
        "ko": "257"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "24",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "43",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60513",
        "ok": "85",
        "ko": "60571"
    },
    "percentiles4": {
        "total": "61357",
        "ok": "170",
        "ko": "61451"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 164,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2002,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.141",
        "ko": "1.723"
    }
}
    },"req_gotocheckout-2f4e2": {
        type: "REQUEST",
        name: "goToCheckout",
path: "goToCheckout",
pathFormatted: "req_gotocheckout-2f4e2",
stats: {
    "name": "goToCheckout",
    "numberOfRequests": {
        "total": "2166",
        "ok": "164",
        "ko": "2002"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "2074"
    },
    "maxResponseTime": {
        "total": "62123",
        "ok": "764",
        "ko": "62123"
    },
    "meanResponseTime": {
        "total": "50176",
        "ok": "130",
        "ko": "54275"
    },
    "standardDeviation": {
        "total": "21860",
        "ok": "92",
        "ko": "17176"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "107",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "174",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60256",
        "ok": "279",
        "ko": "60301"
    },
    "percentiles4": {
        "total": "61141",
        "ok": "363",
        "ko": "61157"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 164,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2002,
    "percentage": 92
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.141",
        "ko": "1.723"
    }
}
    },"req_placeorder-051f2": {
        type: "REQUEST",
        name: "placeOrder",
path: "placeOrder",
pathFormatted: "req_placeorder-051f2",
stats: {
    "name": "placeOrder",
    "numberOfRequests": {
        "total": "2166",
        "ok": "159",
        "ko": "2007"
    },
    "minResponseTime": {
        "total": "96",
        "ok": "96",
        "ko": "1041"
    },
    "maxResponseTime": {
        "total": "61859",
        "ok": "1008",
        "ko": "61859"
    },
    "meanResponseTime": {
        "total": "48603",
        "ok": "324",
        "ko": "52428"
    },
    "standardDeviation": {
        "total": "23062",
        "ok": "167",
        "ko": "19357"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "278",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "428",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60112",
        "ok": "619",
        "ko": "60138"
    },
    "percentiles4": {
        "total": "60928",
        "ok": "876",
        "ko": "60940"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 154,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2007,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.137",
        "ko": "1.727"
    }
}
    },"req_emptycart-b4d4b": {
        type: "REQUEST",
        name: "emptyCart",
path: "emptyCart",
pathFormatted: "req_emptycart-b4d4b",
stats: {
    "name": "emptyCart",
    "numberOfRequests": {
        "total": "2166",
        "ok": "162",
        "ko": "2004"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "2073"
    },
    "maxResponseTime": {
        "total": "62120",
        "ok": "668",
        "ko": "62120"
    },
    "meanResponseTime": {
        "total": "47729",
        "ok": "115",
        "ko": "51578"
    },
    "standardDeviation": {
        "total": "23683",
        "ok": "94",
        "ko": "20202"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "86",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "134",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60025",
        "ok": "280",
        "ko": "60030"
    },
    "percentiles4": {
        "total": "60476",
        "ok": "470",
        "ko": "60514"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 162,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2004,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.139",
        "ko": "1.725"
    }
}
    },"req_getcartproducts-9f1aa": {
        type: "REQUEST",
        name: "getCartProductsAfterEmptying",
path: "getCartProductsAfterEmptying",
pathFormatted: "req_getcartproducts-9f1aa",
stats: {
    "name": "getCartProductsAfterEmptying",
    "numberOfRequests": {
        "total": "2166",
        "ok": "162",
        "ko": "2004"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "2214"
    },
    "maxResponseTime": {
        "total": "61857",
        "ok": "474",
        "ko": "61857"
    },
    "meanResponseTime": {
        "total": "50274",
        "ok": "76",
        "ko": "54332"
    },
    "standardDeviation": {
        "total": "21728",
        "ok": "70",
        "ko": "17033"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "54",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "85",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60019",
        "ok": "204",
        "ko": "60023"
    },
    "percentiles4": {
        "total": "60468",
        "ok": "385",
        "ko": "60502"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 162,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2004,
    "percentage": 93
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.864",
        "ok": "0.139",
        "ko": "1.725"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
