package sharedDB

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class sharedDbSimulation3Buy4ProductsUSD extends Simulation {
	
	val jsonProductFeeder = jsonFile("productList.json").circular
	val jsonCartFeeder = jsonFile("cartList.json").circular
	
	val httpProtocol = http
		.baseUrl("http://localhost:8080")
		.inferHtmlResources()
		.acceptHeader("application/json, text/plain, */*")
		.acceptEncodingHeader("gzip, deflate")
		.authorizationHeader("Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU4MjU2NTEyN30.8VD28M9DPy0gVu7JgwZSiXaiOwXf5Gx_hagxuOGTd1BOjI6azqIJerOPsVDH-0qFl0k4bRSZKhMFRsdmScnQeA")
		.acceptLanguageHeader("de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36")

	val headers_0 = Map(
		"Content-Type" -> "application/json",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_1 = Map(
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_4 = Map(
		"Content-Type" -> "application/json",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_14 = Map(
		"Content-Type" -> "text/plain",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_16 = Map(
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val myJsonHeader = Map(
		"Content-Type" -> "application/json",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		)

	val scn = scenario("simulation3LoginBuy4ProductsUSD")
		
		.feed(jsonProductFeeder, 4)
		.feed(jsonCartFeeder)
		
		
		.exec(http("goToProductCatalogue")
		.get("/services/productcatalogue/api/products?page=0&size=20&sort=id,asc")
			.headers(headers_1))
			
		.pause(100 milliseconds)
		
		.exec(http("getCartsInProductCatalogue")
			.get("/services/cart/api/carts")
			.headers(headers_1)
  		)
			
		.pause(100 milliseconds)
		
		.exec(http("addProductToCart01")
			.post("/services/cart/api/carts/addProduct/${cartId}")
			.headers(headers_4)
  		
			.body(StringBody("""{"id":"${id1}","price":${price1}}""")))
			
		.pause(100 milliseconds)
		
    .exec(http("addProductToCart02")
			.post("/services/cart/api/carts/addProduct/${cartId}")
			.headers(headers_4)
  		
			.body(StringBody("""{"id":"${id2}","price":${price2}}""")))
			
		.pause(100 milliseconds)
		
    .exec(http("addProductToCart03")
			.post("/services/cart/api/carts/addProduct/${cartId}")
			.headers(headers_4)
  		
			.body(StringBody("""{"id":"${id3}","price":${price3}}""")))
			
		.pause(100 milliseconds)
		
    .exec(http("addProductToCart04")
			.post("/services/cart/api/carts/addProduct/${cartId}")
			.headers(headers_4)
  		
			.body(StringBody("""{"id":"${id4}","price":${price4}}""")))
			
		.pause(100 milliseconds)
		
		.exec(http("goToCartGetCarts")
			.get("/services/cart/api/carts")
			.headers(headers_1)
  		)
			
		.pause(100 milliseconds)
		
		.exec(http("cartsGetCartProducts")
			.get("/services/cart/api/carts/getCartProducts/${cartId}")
			.headers(headers_1)
  		)
			
		.pause(100 milliseconds)
		
		.exec(http("goToCheckout")
			.post("/services/checkout/api/go-to-checkout")
			.headers(headers_14)
  		
			.body(StringBody("${cartId}")))
			
		.pause(100 milliseconds)
		
		.exec(http("placeOrder")
			.post("/services/checkout/api/place-order")
			.headers(headers_4)
  		
			.body(StringBody("""{"cartId":"${cartId}","currency":"USD","checkoutPerson":{"firstName":"Gatling Test 4","lastName":"Gatling Test 4","eMailAddress":"gatling@test.com","address":"Test street 1","creditCardNumber":123,"creditCardSecurityNumber":123,"creditCardExpirationDate":202011}}""")))
			
		.pause(100 milliseconds)
		
		.exec(http("emptyCart")
			.delete("/services/checkout/api/empty-cart/${cartId}")
			.headers(headers_16)
  		)
			
		.pause(100 milliseconds)
		
    .exec(http("getCartProductsAfterEmptying")
			.post("/services/checkout/api/go-to-checkout")
			.headers(headers_14)
  		
			.body(StringBody("${cartId}")))
			

	setUp(scn.inject(
		constantConcurrentUsers(1) during (20 seconds),
		rampConcurrentUsers(1) to (5) during (20 seconds),
		constantConcurrentUsers(5) during (20 seconds),
		rampConcurrentUsers(5) to (10) during (20 seconds),
		constantConcurrentUsers(10) during (10 seconds),
		rampConcurrentUsers(10) to (20) during (10 seconds),
		constantConcurrentUsers(20) during(10 seconds),
		rampConcurrentUsers (20) to (50) during (20 seconds),
		constantConcurrentUsers(50) during (20 seconds),
		rampConcurrentUsers (50) to (100) during (20 seconds),
		constantConcurrentUsers(100) during (30 seconds),
		rampConcurrentUsers (100) to (200) during (30 seconds),
		constantConcurrentUsers(200) during (30 seconds),
		rampConcurrentUsers (200) to (500) during (30 seconds),
		constantConcurrentUsers(500) during (30 seconds),
		rampConcurrentUsers(500) to (1000) during (30 seconds),
		constantConcurrentUsers(1000) during (30 seconds),
		rampConcurrentUsers(1000) to (2000) during (60 seconds),
		constantConcurrentUsers(2000) during (60 seconds)
	)).protocols(httpProtocol)
}