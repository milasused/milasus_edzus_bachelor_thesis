package sharedDB

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._


class sharedDbBuyProduct extends Simulation {

	val jsonProductFeeder = jsonFile("productList.json").circular
	val jsonCartFeeder = jsonFile("cartList.json").circular
	
	val httpProtocol = http
		.baseUrl("http://localhost:8080")
		.inferHtmlResources()
		.acceptHeader("application/json, text/plain, */*")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7")
		.authorizationHeader("Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU4MjU2NTEyN30.8VD28M9DPy0gVu7JgwZSiXaiOwXf5Gx_hagxuOGTd1BOjI6azqIJerOPsVDH-0qFl0k4bRSZKhMFRsdmScnQeA")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36")

	val headers_0 = Map(
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_2 = Map(
		"Content-Type" -> "application/json",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_10 = Map(
		"Content-Type" -> "text/plain",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val headers_12 = Map(
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin")

	val myJsonHeader = Map(
		"Content-Type" -> "application/json",
		"Origin" -> "http://localhost:8080",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		)

	val scn = scenario("SharedDBBuy1Product")
		
		.feed(jsonProductFeeder)
		.feed(jsonCartFeeder)
		
		.exec(http("goToProductCatalogue")
			.get("/services/productcatalogue/api/products?page=0&size=20&sort=id,asc")
			.headers(headers_0))
		
		.pause(100 milliseconds)
		
		.exec(http("addProduct")
			.post("/services/cart/api/carts/addProduct/${cartId}")
			.headers(headers_2)
			.body(StringBody("""{"id":"${id}","price":${price}}""")))
			
		.pause(100 milliseconds)
		
		.exec(http("goToCheckout")
			.post("/services/checkout/api/go-to-checkout")
			.headers(headers_10)
			.body(StringBody("${cartId}")))
			
		.pause(100 milliseconds)
		
		.exec(http("placeOrder")
			.post("/services/checkout/api/place-order")
			.headers(headers_2)
			
		.body(StringBody("""{"cartId":"${cartId}","currency":"EUR","checkoutPerson":{"firstName":"Max","lastName":"Gatling","eMailAddress":"gatling@test.com","address":"Test Strasse 1","creditCardNumber":111,"creditCardSecurityNumber":123,"creditCardExpirationDate":202012}}""")))
			
		.pause(100 milliseconds)
		
		.exec(http("emptyCart")
			.delete("/services/checkout/api/empty-cart/${cartId}")
			.headers(headers_12))

		
	setUp(
		scn.inject(
			constantConcurrentUsers(1) during (20 seconds),
			rampConcurrentUsers(1) to (5) during (20 seconds),
			constantConcurrentUsers(5) during (20 seconds),
			rampConcurrentUsers(5) to (10) during (20 seconds),
			constantConcurrentUsers(10) during (10 seconds),
			rampConcurrentUsers(10) to (20) during (10 seconds),
			constantConcurrentUsers(20) during(10 seconds),
			rampConcurrentUsers (20) to (50) during (20 seconds),
			constantConcurrentUsers(50) during (20 seconds),
			rampConcurrentUsers (50) to (100) during (20 seconds),
			constantConcurrentUsers(100) during (30 seconds),
			rampConcurrentUsers (100) to (200) during (30 seconds),
			constantConcurrentUsers(200) during (30 seconds),
			rampConcurrentUsers (200) to (500) during (30 seconds),
			constantConcurrentUsers(500) during (30 seconds),
			rampConcurrentUsers(500) to (1000) during (30 seconds),
			constantConcurrentUsers(1000) during (30 seconds),
			rampConcurrentUsers(1000) to (2000) during (60 seconds),
			constantConcurrentUsers(2000) during (60 seconds)
	)).protocols(httpProtocol)
}