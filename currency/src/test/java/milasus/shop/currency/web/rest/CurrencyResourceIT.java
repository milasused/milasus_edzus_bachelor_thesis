package milasus.shop.currency.web.rest;

import milasus.shop.currency.CurrencyApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the CurrencyResource REST controller.
 *
 * @see CurrencyResourceCalculator
 */
@SpringBootTest(classes = CurrencyApp.class)
public class CurrencyResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        CurrencyResourceCalculator currencyResource = new CurrencyResourceCalculator();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(currencyResource)
            .build();
    }

    /**
     * Test calculateOtherCurrencyProductList
     */
    @Test
    public void testCalculateOtherCurrencyProductList() throws Exception {
        restMockMvc.perform(post("/api/currency/calculate-other-currency-product-list"))
            .andExpect(status().isOk());
    }

    /**
     * Test calculateOtherCurrencyValue
     */
    @Test
    public void testCalculateOtherCurrencyValue() throws Exception {
        restMockMvc.perform(get("/api/currency/calculate-other-currency-value"))
            .andExpect(status().isOk());
    }
}
