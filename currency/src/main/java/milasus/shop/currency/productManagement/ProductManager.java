package milasus.shop.currency.productManagement;

import milasus.shop.currency.exception.CurrencyAmountNotFoundRuntimeException;
import milasus.shop.currency.exception.CurrencyNotFoundRuntimeException;
import milasus.shop.currency.exception.CurrencyProductRuntimeException;
import product.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductManager {

    private Integer exchangeRatio;

    public ProductManager(Integer exchangeRatio) {
        this.exchangeRatio = exchangeRatio;
    }

    /**
     * calculate new prices for a list of products
     * @param productList
     * @return
     */
    public List<Product> calculateProductListPrices(List<Product> productList) {

        List<Product> returnProductList = new ArrayList<Product>();

        for(Product product: productList){
            Product newProduct = new Product(product.getId(),
                this.calculateOtherCurrency(product.getPrice()));
            returnProductList.add(newProduct);
        }

        return returnProductList;
    }

    /**
     * Exchange currency calculation
     * @param value
     * @return
     */
    public Integer calculateOtherCurrency(Integer value){
        return value * exchangeRatio / 100;
    }

    /**
     * Check if product has ID and price set
     * @param product
     */
    public void productCheck(Product product){
        if(product.getId() == null){
            throw new CurrencyProductRuntimeException("Product ID must not be null");
        }
        this.valueCheck(product.getPrice());
    }

    /**
     * Check if currency is valid
     * @param currency
     */
    public void currencyCheck (String currency){
        if(currency == null){
            throw new CurrencyNotFoundRuntimeException(currency + " is not found as currency");
        }
    }

    /**
     * Check if value meets contstraints
     * @param value
     */
    public void valueCheck(Integer value) {
        if(value == null || value < 0){
            throw new CurrencyAmountNotFoundRuntimeException(value + " is an invalid amount to calculate");
        }
    }

    public Integer getExchangeRatio() {
        return exchangeRatio;
    }

    public void setExchangeRatio(Integer exchangeRatio) {
        this.exchangeRatio = exchangeRatio;
    }
}
