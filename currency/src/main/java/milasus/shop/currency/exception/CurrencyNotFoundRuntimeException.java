package milasus.shop.currency.exception;

public class CurrencyNotFoundRuntimeException extends RuntimeException {
    public CurrencyNotFoundRuntimeException(String message) {
        super(message);
    }
}
