package milasus.shop.currency.exception;

public class CurrencyAmountNotFoundRuntimeException extends RuntimeException {

    public CurrencyAmountNotFoundRuntimeException(String errorMessage){
        super(errorMessage);
    }
}
