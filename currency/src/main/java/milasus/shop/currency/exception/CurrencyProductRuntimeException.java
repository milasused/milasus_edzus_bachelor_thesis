package milasus.shop.currency.exception;

public class CurrencyProductRuntimeException extends RuntimeException {
    public CurrencyProductRuntimeException(String message) {
        super(message);
    }
}
