/**
 * View Models used by Spring MVC REST controllers.
 */
package milasus.shop.currency.web.rest.vm;
