package milasus.shop.currency.web.rest;

import milasus.shop.currency.exception.CurrencyAmountNotFoundRuntimeException;
import milasus.shop.currency.exception.CurrencyNotFoundRuntimeException;
import milasus.shop.currency.exception.CurrencyProductRuntimeException;
import milasus.shop.currency.productManagement.ProductManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import product.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * CurrencyResource controller
 */
@RestController
@RequestMapping("/api/currency")
public class CurrencyResourceCalculator {

    private final Logger log = LoggerFactory.getLogger(CurrencyResourceCalculator.class);
    private ProductManager productManager;

    /**
    * POST calculateOtherCurrencyProductList
    */
    @PostMapping("/calculate-other-currency-product-list/{currency}")
    public List<Product> calculateOtherCurrencyProductList(@PathVariable String currency,
                                               @RequestBody List<Product> productList) {

        this.setupProductManager();

        this.productManager.currencyCheck(currency);
        for(Product product: productList){
            this.productManager.productCheck(product);
        }

        List<Product> newProductPrices =
                this.productManager.calculateProductListPrices(productList);

        return newProductPrices;
    }


    /**
    * GET calculateOtherCurrencyValue
    */
    @PostMapping("/calculate-other-currency-value/{currency}")
    public Integer calculateOtherCurrencyValue(@PathVariable String currency,
                                         @RequestBody Integer value) {

        this.setupProductManager();
        this.productManager.currencyCheck(currency);
        this.productManager.valueCheck(value);

        return this.productManager.calculateOtherCurrency(value);
    }

    private void setupProductManager() {
        if(productManager == null){
            productManager = new ProductManager(120);
        }
    }

}
